package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.model.Relation;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Ints;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasItem;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class RelatednessDBTestBase {


    protected abstract RelStore getRelatednessDB();

    @Test
    public void testAddRetrieve() {
        RelStore reldb = getRelatednessDB();

        String uri1 = "http://dbpedia.org/resource/Paris";
        String uri2 = "http://dbpedia.org/resource/Berlin";

        Relation r1 = new Relation(uri1, uri2, 0.1);

        reldb.addRelatedness(r1);

        double dr2 = reldb.getRelatedness(uri1, uri2);
        Assert.assertTrue(dr2 > 0);
        Assert.assertEquals(r1.getRelatedness(),dr2,0.01);

        double dr3 = reldb.getRelatedness(uri2, uri1);
        Assert.assertTrue(dr3 > 0);
        Assert.assertEquals(r1.getRelatedness(),dr3, 0.01);
    }


    @Test
    public void testQuery() {
        RelStore reldb = getRelatednessDB();

        String uri1 = "http://dbpedia.org/resource/Paris";
        String uri2 = "http://dbpedia.org/resource/Berlin";
        String uri3 = "http://dbpedia.org/resource/Vienna";

        Relation r1 = new Relation(uri1, uri2, 0.1);
        Relation r2 = new Relation(uri1, uri3, 0.1);

        reldb.addRelatedness(r1);
        reldb.addRelatedness(r2);

        // query everything with uri1, should be two results (r1 and r2)
        List<Integer> relsUri1 = Ints.asList(reldb.getAdjacent(uri1));
        Assert.assertEquals(2, relsUri1.size());
        Assert.assertThat(relsUri1, hasItem(reldb.getNodeID(uri2)));
        Assert.assertThat(relsUri1, hasItem(reldb.getNodeID(uri3)));

        List<Integer> relsUri2 = Ints.asList(reldb.getAdjacent(uri2));
        Assert.assertEquals(1, relsUri2.size());
        Assert.assertThat(relsUri2, hasItem(reldb.getNodeID(uri1)));

        List<Integer> relsUri3 = Ints.asList(reldb.getAdjacent(uri3));
        Assert.assertEquals(1, relsUri3.size());
        Assert.assertThat(relsUri3, hasItem(reldb.getNodeID(uri1)));
    }

}
