package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreMemory;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessDBMemoryTest extends RelatednessDBTestBase {

    @Override
    protected RelStore getRelatednessDB() {
        return new RelStoreMemory();
    }
}
