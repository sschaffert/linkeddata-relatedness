package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.model.Relation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelationTest {

    @Test
    public void testRelationSymmetry() {
        String uri1 = "http://dbpedia.org/resource/Paris";
        String uri2 = "http://dbpedia.org/resource/Berlin";

        Relation r1 = new Relation(uri1, uri2, 0.0);
        Relation r2 = new Relation(uri2, uri1, 0.0);

        Assert.assertEquals(r1,r2);
        Assert.assertEquals(r1.getConceptUri1(), r2.getConceptUri1());
        Assert.assertEquals(r1.getConceptUri2(), r2.getConceptUri2());
    }
}
