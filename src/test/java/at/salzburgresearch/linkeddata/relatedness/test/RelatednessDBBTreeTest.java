package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreBTree;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;

import java.io.File;
import java.io.IOException;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessDBBTreeTest extends RelatednessDBTestBase {

    File tempDir;
    RelStoreBTree reldb;

    @Before
    public void setup() throws IOException {
        tempDir = Files.createTempDir();
        reldb   = new RelStoreBTree(tempDir, "test");
    }

    @After
    public void teardown() throws IOException {
        FileUtils.deleteDirectory(tempDir);
    }

    @Override
    protected RelStore getRelatednessDB() {
        return reldb;
    }
}
