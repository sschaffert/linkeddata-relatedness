package at.salzburgresearch.linkeddata.relatedness.test.btree;

import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedIntArrays;
import com.google.common.io.Files;
import com.google.common.primitives.Ints;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntArraysTest {

    private static Logger log = LoggerFactory.getLogger(MappedIntArraysTest.class);

    File tempDir;
    MappedIntArrays arrays;

    int[] bigArray1, smallArray1, bigArray2, smallArray2;

    @Before
    public void setup() throws IOException {
        tempDir = Files.createTempDir();
        arrays = new MappedIntArrays(tempDir,"test");

        // allocate two int arrays, the first of a length bigger than the current block size, the second smaller
        bigArray1 = new int[66];
        for(int i=0; i< bigArray1.length; i++) {
            bigArray1[i] = i;
        }

        smallArray1 = new int[33];
        for(int i=0; i< smallArray1.length; i++) {
            smallArray1[i] = i;
        }

        bigArray2 = new int[99];
        for(int i=0; i< bigArray2.length; i++) {
            bigArray2[i] = i;
        }

        smallArray2 = new int[22];
        for(int i=0; i< smallArray2.length; i++) {
            smallArray2[i] = i;
        }
    }


    @After
    public void teardown() throws IOException {
        FileUtils.deleteDirectory(tempDir);
    }

    @Test
    public void testSetGet() throws IOException {

        long pos1 = arrays.newArray(bigArray1);
        long pos2 = arrays.newArray(smallArray1);

        Assert.assertEquals((128*4 + 8) + (64*4 + 8), arrays.getEndPointer());

        // test position
        Assert.assertEquals(0L, pos1);
        Assert.assertEquals((128*4 + 8), pos2);

        // test capacity
        Assert.assertEquals(128, arrays.getCapacity(pos1));
        Assert.assertEquals(64, arrays.getCapacity(pos2));

        // test length
        Assert.assertEquals(66, arrays.getLength(pos1));
        Assert.assertEquals(33, arrays.getLength(pos2));


        // test content
        Assert.assertArrayEquals(bigArray1, arrays.getArray(pos1));
        Assert.assertArrayEquals(smallArray1, arrays.getArray(pos2));

        System.err.println(arrays);
    }

    @Test
    public void testUpdate() throws IOException {

        // we store the small array first, then a large one
        long smallPos1 = arrays.newArray(smallArray1);
        long largePos  = arrays.newArray(bigArray1);

        // test content
        Assert.assertArrayEquals(bigArray1, arrays.getArray(largePos));
        Assert.assertArrayEquals(smallArray1, arrays.getArray(smallPos1));

        // now we update the small array so it is larger than the block size
        long smallPos2 = arrays.updateArray(smallPos1, bigArray2);

        // should be a new position
        Assert.assertNotEquals(smallPos1,smallPos2);

        Assert.assertArrayEquals(bigArray2, arrays.getArray(smallPos2));

    }


    @Test
    public void testAlloc() throws IOException {
        // allocate a small array first, then a large one
        long smallPos1 = arrays.newArray(smallArray1);
        long largePos  = arrays.newArray(bigArray1);

        // update the small one to a big one, so it will be relocated
        long smallPos2 = arrays.updateArray(smallPos1, bigArray2);

        // request a new small array
        long smallPos3 = arrays.newArray(smallArray2);

        // should now be at the position of the last small array
        Assert.assertEquals(smallPos1,smallPos3);

        // test content
        Assert.assertArrayEquals(smallArray2, arrays.getArray(smallPos3));

        System.err.println(arrays);
    }



    @Test
    public void testLarge() throws IOException {
        int count = 10000;

        long[] positions = new long[count];

        long insStart = System.currentTimeMillis();

        // allocate a number of arrays of varying size
        for(int i=1; i<count; i++) {
            int[] val = new int[i];
            for(int j = 0; j<i; j++) {
                val[j] = j;
            }

            positions[i] = arrays.newArray(val);
        }

        log.info("inserting {} arrays took {} ms", count, System.currentTimeMillis() - insStart);

        long retStart = System.currentTimeMillis();

        // test values
        for(int i=1; i<count; i++) {
            int[] val = arrays.getArray(positions[i]);
            Assert.assertEquals(i,val.length);
        }

        log.info("retrieving {} arrays took {} ms", count, System.currentTimeMillis() - retStart);

        System.err.println(arrays);

    }


    @Test
    public void testCopy() throws IOException {
        int count = 10000;

        long[] positions = new long[count];

        long insStart = System.currentTimeMillis();

        // allocate a number of arrays of varying size
        for(int i=1; i<count; i++) {
            int[] val = new int[i];
            for(int j = 0; j<i; j++) {
                val[j] = j;
            }

            positions[i] = arrays.newArray(val);
        }

        log.info("inserting {} arrays took {} ms", count, System.currentTimeMillis() - insStart);

        long cpyStart = System.currentTimeMillis();

        MappedIntArrays copy = new MappedIntArrays(tempDir,"copy");
        arrays.copyTo(copy);

        log.info("copying took {} ms", System.currentTimeMillis() - cpyStart);

        long retStart = System.currentTimeMillis();

        // test values
        for(int i=1; i<count; i++) {
            int[] val = copy.getArray(positions[i]);
            Assert.assertEquals(i,val.length);
        }

        log.info("retrieving {} arrays took {} ms", count, System.currentTimeMillis() - retStart);

    }


    @Test
    public void testSearch() throws IOException {
        int[] sorted = {1,3,5};

        long pos = arrays.newArray(sorted);

        Assert.assertEquals(3, arrays.getLength(pos));

        Assert.assertEquals(1, arrays.binarySearch(pos, 3));
        Assert.assertEquals(0, arrays.binarySearch(pos, 1));
        Assert.assertEquals(2, arrays.binarySearch(pos, 5));
        Assert.assertEquals(-3, arrays.binarySearch(pos, 4));
        Assert.assertEquals(-2, arrays.binarySearch(pos, 2));

    }


    @Test
    public void insertAtSmall() throws IOException {
        int[] sorted = {1,3,5};

        long pos = arrays.newArray(sorted);

        arrays.insertAt(pos,2,1);

        Assert.assertEquals(4, arrays.getLength(pos));

        int[] expected = {1,2,3,5};

        Assert.assertArrayEquals(expected, arrays.getArray(pos));
    }

    @Test
    public void insertAtLarge() throws IOException {
        int count = 60;

        int[] sorted = new int[count];
        for(int i=0; i<count; i++) {
            sorted[i] = i*2;
        }
        long pos = arrays.newArray(sorted);

        // interweave ...
        for(int i=0; i<count; i++) {
            pos = arrays.insertAt(pos,(i*2)+1,i*2+1);
        }

        Assert.assertEquals(count * 2, arrays.getLength(pos));

        int[] expected = new int[count*2];
        for(int i=0; i<count*2; i++) {
            expected[i] = i;
        }

        Assert.assertArrayEquals(expected, arrays.getArray(pos));
    }


    @Test
    public void testCompare() {

        int i1 = -10;
        int i2 = 0;
        int i3 = 10;


        Assert.assertEquals(0, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i1), Ints.toByteArray(i1)));
        Assert.assertEquals(0, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i2), Ints.toByteArray(i2)));
        Assert.assertEquals(0, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i3), Ints.toByteArray(i3)));

        Assert.assertEquals(-1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i1), Ints.toByteArray(i2)));
        Assert.assertEquals(-1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i2), Ints.toByteArray(i3)));
        Assert.assertEquals(-1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i1), Ints.toByteArray(i3)));
        Assert.assertEquals(1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i2), Ints.toByteArray(i1)));
        Assert.assertEquals(1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i3), Ints.toByteArray(i2)));
        Assert.assertEquals(1, MappedIntArrays.compareArraysSigned(Ints.toByteArray(i3), Ints.toByteArray(i1)));


    }

}
