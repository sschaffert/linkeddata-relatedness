package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.algorithm.RelatednessAlgorithmRepository;
import at.salzburgresearch.linkeddata.relatedness.factory.MapDBFactory;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessAlgorithmTest {

    private final static String NS_FOAF = "http://xmlns.com/foaf/0.1/";
    private final static String NS_LOCAL = "http://localhost/myont/";

    private final static String P1 = NS_LOCAL + "P1";
    private final static String P2 = NS_LOCAL + "P2";
    private final static String P3 = NS_LOCAL + "P3";
    private final static String P4 = NS_LOCAL + "P4";

    DB db;
    File dbfile;

    Repository repository;

    @Before
    public void setup() throws IOException, RepositoryException, RDFParseException {
        dbfile = Files.createTempFile("reldb", ".db").toFile();
        db = DBMaker
                .newFileDB(dbfile)
                .closeOnJvmShutdown()
                .cacheLRUEnable()
                .cacheSize(5000)
                .make();

        repository = new SailRepository(new MemoryStore());
        repository.initialize();

        // load data
        RepositoryConnection con = repository.getConnection();
        try {
            con.add(this.getClass().getResourceAsStream("data.ttl"), NS_LOCAL, RDFFormat.TURTLE);
            con.commit();
        } catch(RepositoryException ex) {
            con.rollback();
        } finally {
            con.close();
        }
    }

    @After
    public void teardown() throws IOException, RepositoryException {
        db.close();
        Files.delete(dbfile.toPath());
        Files.delete(Paths.get(dbfile.toPath().toString() + ".p"));
        Files.delete(Paths.get(dbfile.toPath().toString() + ".t"));

        repository.shutDown();
    }



    @Test
    public void testOnePass() throws Exception {
        RelatednessAlgorithmRepository ldr = new RelatednessAlgorithmRepository(new MapDBFactory(db), repository, ImmutableMap.of(NS_FOAF + "knows", 0.8));

        RelStore reldb = ldr.run(0);

        Assert.assertFalse(reldb.isEmpty());

        Map<Integer,Double> map = new HashMap<Integer, Double>();

        reldb.exportTo(map);

        // check for expected values
        Assert.assertEquals(0.8, map.get(P1.hashCode() * P2.hashCode()), 0.01);
        Assert.assertEquals(0.8, map.get(P1.hashCode() * P3.hashCode()), 0.01);
        Assert.assertNull(map.get(P1.hashCode() * P4.hashCode()));

    }

    @Test
    public void testTwoPass() throws Exception {
        RelatednessAlgorithmRepository ldr = new RelatednessAlgorithmRepository(new MapDBFactory(db), repository, ImmutableMap.of(NS_FOAF + "knows", 0.8));

        RelStore reldb = ldr.run(1);

        Assert.assertFalse(reldb.isEmpty());

        Map<Integer,Double> map = new HashMap<Integer, Double>();

        reldb.exportTo(map);

        // check for expected values
        Assert.assertEquals(0.8, map.get(P1.hashCode() * P2.hashCode()), 0.01);
        Assert.assertEquals(0.8, map.get(P1.hashCode() * P3.hashCode()), 0.01);
        Assert.assertEquals(0.8*0.8, reldb.getRelatedness(P1, P4), 0.01);
        Assert.assertEquals(0.8 * 0.8, map.get(P1.hashCode() * P4.hashCode()), 0.01);

    }

}
