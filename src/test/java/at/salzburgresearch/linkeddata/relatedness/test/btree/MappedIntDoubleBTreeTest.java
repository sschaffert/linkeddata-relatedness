package at.salzburgresearch.linkeddata.relatedness.test.btree;

import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedIntDoubleBTree;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntDoubleBTreeTest {

    private static Logger log = LoggerFactory.getLogger(MappedIntDoubleBTreeTest.class);

    File tempDir;
    MappedIntDoubleBTree tree;

    @Before
    public void setup() throws IOException {
        tempDir = Files.createTempDir();
        tree = new MappedIntDoubleBTree(tempDir,"test", 4);
    }

    @After
    public void teardown() throws IOException {
        FileUtils.deleteDirectory(tempDir);
    }

    @Test
    public void testGetPut() throws IOException {
        // add three values and try to retrieve them
        double put1 = tree.put(2, 0.1);
        double put2 = tree.put(1, 0.2);
        double put3 = tree.put(3, 0.3);

        System.err.println(tree);


        Assert.assertEquals(Double.NaN, put1, 0.01);
        Assert.assertEquals(Double.NaN, put2, 0.01);
        Assert.assertEquals(Double.NaN, put3, 0.01);

        byte[] treebuf = tree.toByteArray();

        // check individual bytes

        // bytes 0-7 contain a long pointing to the root node, which is located at position 16
        Assert.assertEquals((byte)16, treebuf[7]);

        // bytes 8-15 contain a long pointing to the end of the buffer, should be located at 16+totalNodeSize
        byte[] endPointer = Arrays.copyOfRange(treebuf,8,16);
        Assert.assertEquals(16+tree.getTotalNodeSize(), Longs.fromByteArray(endPointer));

        // bytes 16 onwards should contain the root node in the following form
        // (long)   - leftmost pointer, should be 0L
        // (int)    - first key, should be 1
        // (double) - first double, should be 0.2
        // (long)   - intermediate link, should be 0
        byte[] firstLink = Arrays.copyOfRange(treebuf, 16, 24);
        Assert.assertEquals(0L, Longs.fromByteArray(firstLink));

        byte[] firstKey = Arrays.copyOfRange(treebuf, 24, 28);
        Assert.assertEquals(1, Ints.fromByteArray(firstKey));

        byte[] firstVal = Arrays.copyOfRange(treebuf, 28, 36);
        Assert.assertEquals(0.2, Double.longBitsToDouble(Longs.fromByteArray(firstVal)), 0.01);


        byte[] secondLink = Arrays.copyOfRange(treebuf, 36, 44);
        Assert.assertEquals(0L, Longs.fromByteArray(secondLink));

        byte[] secondKey = Arrays.copyOfRange(treebuf, 44, 48);
        Assert.assertEquals(2, Ints.fromByteArray(secondKey));

        byte[] secondVal = Arrays.copyOfRange(treebuf, 48, 56);
        Assert.assertEquals(0.1, Double.longBitsToDouble(Longs.fromByteArray(secondVal)), 0.01);



        Assert.assertEquals(0.1, tree.get(2), 0.01);
        Assert.assertEquals(0.2, tree.get(1), 0.01);
        Assert.assertEquals(0.3, tree.get(3), 0.01);
    }

    /**
     * Test inserting the 0 value
     * @throws IOException
     */
    @Test
    public void testZero() throws IOException {
        tree.put(0, 1.0);

        Assert.assertEquals(1.0, tree.get(0), 0.01);
    }

    /**
     * Insert more values than fit into a node and test if split worked correctly
     * @throws IOException
     */
    @Test
    public void testSplit() throws IOException {
        // add six values and try to retrieve them
        tree.put(5, 0.5);
        tree.put(1, 0.1);
        tree.put(3, 0.3);
        tree.put(7, 0.7);
        tree.put(4, 0.4);
        tree.put(6, 0.6);
        tree.put(8, 0.8);
        tree.put(2, 0.2);
        tree.put(9, 0.9);
        tree.put(10, 0.1);
        tree.put(11, 0.11);

        System.err.println(tree);



        // the root node should now be located in a new position ( 16 + totalNodeSize)
        byte[] treebuf = tree.toByteArray();

        // check individual bytes

        // bytes 0-7 contain a long pointing to the root node, which is located at position 16
        byte[] rootNode = Arrays.copyOfRange(treebuf, 0, 8);
        Assert.assertEquals(16 + 2*tree.getTotalNodeSize(), Longs.fromByteArray(rootNode));



        Assert.assertEquals(0.1, tree.get(1), 0.01);
        Assert.assertEquals(0.2, tree.get(2), 0.01);
        Assert.assertEquals(0.3, tree.get(3), 0.01);
        Assert.assertEquals(0.4, tree.get(4), 0.01);
        Assert.assertEquals(0.5, tree.get(5), 0.01);
        Assert.assertEquals(0.6, tree.get(6), 0.01);
        Assert.assertEquals(0.7, tree.get(7), 0.01);
        Assert.assertEquals(0.8, tree.get(8), 0.01);
        Assert.assertEquals(0.9, tree.get(9), 0.01);
        Assert.assertEquals(0.1, tree.get(10), 0.01);
        Assert.assertEquals(0.11, tree.get(11), 0.01);

    }


    /**
     * Insert more values than fit into a node and test if split worked correctly
     * @throws IOException
     */
    @Test
    public void testNegative() throws IOException {
        // add six values and try to retrieve them
        tree.put(-5, 0.5);
        tree.put(1, 0.1);
        tree.put(3, 0.3);
        tree.put(-7, 0.7);
        tree.put(4, 0.4);
        tree.put(6, 0.6);
        tree.put(-8, 0.8);
        tree.put(2, 0.2);
        tree.put(9, 0.9);
        tree.put(10, 0.1);
        tree.put(0, 0.11);

        Assert.assertEquals(0.1, tree.get(1), 0.01);
        Assert.assertEquals(0.2, tree.get(2), 0.01);
        Assert.assertEquals(0.3, tree.get(3), 0.01);
        Assert.assertEquals(0.4, tree.get(4), 0.01);
        Assert.assertEquals(0.5, tree.get(-5), 0.01);
        Assert.assertEquals(0.6, tree.get(6), 0.01);
        Assert.assertEquals(0.7, tree.get(-7), 0.01);
        Assert.assertEquals(0.8, tree.get(-8), 0.01);
        Assert.assertEquals(0.9, tree.get(9), 0.01);
        Assert.assertEquals(0.1, tree.get(10), 0.01);
        Assert.assertEquals(0.11, tree.get(0), 0.01);

    }


    /**
     * Test a large amount of inserts (1.000.000) and see how the system performs
     */
    @Test
    public void testLarge() throws IOException {
        int count = 1000000;

        long start = System.currentTimeMillis();

        long batchStart = System.currentTimeMillis();
        for(int i=1; i<count; i++) {
            tree.put(i, 1000.0 / i);

            if(i % 100000 == 0) {
                log.info("inserted {} ({}/sec)",i, (100000 * 1000 / (System.currentTimeMillis() - batchStart)));
                batchStart = System.currentTimeMillis();
            }
        }

        log.info("insertion took {} ms, tree size now {}, root node at {}, node count {},  max level {}", System.currentTimeMillis() - start, tree.getEndPointer(), tree.getRootPointer(), tree.getNodeCount(), tree.getMaxLevel());

        start = System.currentTimeMillis();
        // test values
        for(int i=1; i<count; i++) {
            Assert.assertEquals(1000.0/i, tree.get(i), 0.001);
        }
        log.info("retrieval took {} ms", System.currentTimeMillis() - start);

    }


    /**
     * Test updating a value.
     *
     * @throws IOException
     */
    @Test
    public void testUpdate() throws IOException {
        int count = 1000;

        long start = System.currentTimeMillis();

        for(int i=1; i<count; i++) {
            tree.put(i, 1000.0/i);
        }

        log.info("insertion took {} ms, tree size now {}, root node at {}, node count {},  max level {}", System.currentTimeMillis() - start, tree.getEndPointer(), tree.getRootPointer(), tree.getNodeCount(), tree.getMaxLevel());


        double old3 = tree.put(3, 0.123);
        double old123 = tree.put(123, 0.3);

        Assert.assertEquals(0.123, tree.get(3), 0.01);
        Assert.assertEquals(0.3,   tree.get(123), 0.01);
        Assert.assertEquals(1000.0/3, old3, 0.01);
        Assert.assertEquals(1000.0/123, old123, 0.01);

    }



    @Test
    public void testIterator() throws IOException {
        int count = 10000;

        long insStart = System.currentTimeMillis();

        for(int i=1; i<count; i++) {
            tree.put(i, 1000.0/i);
        }

        log.info("insertion took {} ms, tree size now {}, root node at {}, node count {},  max level {}", System.currentTimeMillis() - insStart, tree.getEndPointer(), tree.getRootPointer(), tree.getNodeCount(), tree.getMaxLevel());

        //System.err.println(tree);

        long itStart = System.currentTimeMillis();
        List<MappedIntDoubleBTree.KeyValuePair> entries = Lists.newArrayList(tree);
        log.info("iterating took {} ms, {} entries", System.currentTimeMillis() - itStart, entries.size());

        Assert.assertEquals(count - 1,entries.size());

    }


    @Test
    public void testCopy() throws IOException {
        int count = 10000;

        long insStart = System.currentTimeMillis();

        for(int i=1; i<count; i++) {
            tree.put(i, 1000.0/i);
        }

        log.info("insertion took {} ms, tree size now {}, root node at {}, node count {},  max level {}", System.currentTimeMillis() - insStart, tree.getEndPointer(), tree.getRootPointer(), tree.getNodeCount(), tree.getMaxLevel());

        long cpyStart = System.currentTimeMillis();

        // create a new empty tree with the same parameters and copy the old tree to it
        MappedIntDoubleBTree copy= new MappedIntDoubleBTree(tempDir,"copy", 4);
        tree.copyTo(copy);

        log.info("copying took {} ms, copy size now {}, root node at {}, node count {}, max level {}", System.currentTimeMillis() - cpyStart, copy.getEndPointer(), copy.getRootPointer(), copy.getNodeCount(), copy.getMaxLevel());

        // check if we can find all values in the copy
        long retStart = System.currentTimeMillis();
        // test values
        for(int i=1; i<count; i++) {
            Assert.assertEquals(1000.0/i, copy.get(i), 0.001);
        }
        log.info("retrieval took {} ms", System.currentTimeMillis() - retStart);
    }

}
