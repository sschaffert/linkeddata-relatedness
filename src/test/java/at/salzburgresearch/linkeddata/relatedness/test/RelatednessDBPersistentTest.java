package at.salzburgresearch.linkeddata.relatedness.test;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreMapDB;
import org.junit.After;
import org.junit.Before;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessDBPersistentTest extends RelatednessDBTestBase {

    DB db;
    File dbfile;
    RelStoreMapDB reldb;

    @Before
    public void setup() throws IOException {
        dbfile = Files.createTempFile("reldb", ".db").toFile();
        db = DBMaker
                .newFileDB(dbfile)
                .closeOnJvmShutdown()
                .cacheLRUEnable()
                .cacheSize(5000)
                .make();

        reldb = new RelStoreMapDB(db, "test");
    }

    @After
    public void teardown() throws IOException {
        db.close();
        Files.delete(dbfile.toPath());
        Files.delete(Paths.get(dbfile.toPath().toString() + ".p"));
        Files.delete(Paths.get(dbfile.toPath().toString() + ".t"));
    }

    @Override
    protected RelStore getRelatednessDB() {
        return reldb;
    }
}
