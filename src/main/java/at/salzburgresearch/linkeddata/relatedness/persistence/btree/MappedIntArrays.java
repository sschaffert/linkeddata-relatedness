package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.primitives.Ints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

/**
 * A class for managing a MappedByteBuffer containing int arrays at specified positions; structure is:
 * <ul>
 * <li>4 bytes for representing the capacity C of the int array (i.e. maximum size of the array in this area)</li>
 * <li>4 bytes for representing the size S of the int array to come</li>
 * <li>S * 4 bytes for holding the int array of size S</li>
 * </ul>
 * initial size for each array can be given in the constructor; default is 32.
 * <p/>
 * The class maintains a list of free areas in a separate memory-mapped file. This list
 * is a pair of (long,int) representing the pointer to the position and the size of the free area.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntArrays extends MappedBase {

    private static Logger log = LoggerFactory.getLogger(MappedIntArrays.class);

    private static final long PAGE_SIZE = Integer.MAX_VALUE;  // size of a memory-mapped region
    private static final int  BLOCK_SIZE = 64;                // size of an array block

    private List<MappedByteBuffer> intArrays;


    private TreeMap<Integer, Queue<Long>> freeAreas;

    private FileChannel intArraysChannel; //, freeAreasChannel;

    private long endPointer; // point to the end of the intArrays

    /**
     * Create a new MappedIntArrays instance in the given directory with the given basename for files.
     *
     * @param directory
     * @param basename
     */
    public MappedIntArrays(File directory, String basename) throws IOException {
        if(!directory.exists() || !directory.isDirectory()) {
            throw new IllegalArgumentException("the directory "+directory+" does not exist or is not a directory!");
        } else {
            log.info("creating new memory-mapped file {}/{}.data", directory, basename);
        }

        intArraysChannel = FileChannel.open(new File(directory, basename + ".data").toPath(), StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW, StandardOpenOption.SPARSE, StandardOpenOption.DELETE_ON_CLOSE);

        intArrays = new ArrayList<>();

        // initialise the first page immediately
        intArrays.add(intArraysChannel.map(FileChannel.MapMode.READ_WRITE, 0, PAGE_SIZE));

        freeAreas = new TreeMap<>();
    }

    /**
     * Return the capacity of the block starting at the given position
     *
     * @param position
     * @return
     */
    public int getCapacity(long position) {
        return readInt(position);
    }

    /**
     * Return the length of the array stored in the block starting at the given position
     * @param position
     * @return
     */
    public int getLength(long position) {
        return readInt(position+4);
    }

    protected void setLength(long position, int length) throws IOException {
        writeInt(position+4, length);
    }


    /**
     * A helper method to retrieve bytes across the page size
     * @param position
     * @return
     */
    @Override
    protected byte getByte(long position) {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);

        if(page_number > intArrays.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else {
            return intArrays.get(page_number).get(page_position);
        }
    }

    /**
     * Get the byte array starting at the given position (read-only). The byte array is guaranteed to allow reading
     * at least the specified number of bytes.
     *
     * @param position
     * @return
     * @throws java.io.IOException
     */
    @Override
    protected byte[] getBytes(long position, int length) {
        byte[] result = new byte[length];

        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + length) / PAGE_SIZE);

        if(end_page_number > intArrays.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else if (page_number == end_page_number) {
            // efficient bulk get within page
            ((ByteBuffer)intArrays.get(page_number).position(page_position)).get(result,0,length);
            return result;
        } else {
            // slower iteration over page boundaries
            for(int i=0; i<length; i++) {
                result[i] = getByte(position + i);
            }
            return result;
        }
    }

    /**
     * Return a ByteBuffer view of the data starting at the given position until the given length.
     *
     * @param position
     * @return
     */
    @Override
    protected ByteBuffer getBytesBuffer(long position, int length) {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + length) / PAGE_SIZE);

        if(end_page_number > intArrays.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else if (page_number == end_page_number) {
            // efficient bulk get within page
            ByteBuffer result = ((ByteBuffer)intArrays.get(page_number).position(page_position)).slice();
            result.limit(length);
            return result;
        } else {
            ByteBuffer result = ByteBuffer.allocate(length);
            // slower iteration over page boundaries
            for(int i=0; i<length; i++) {
                result.put(getByte(position + i));
            }
            result.position(0);
            result.limit(length);
            return result;
        }
    }

    /**
     * Set the byte at the given position to the given value, increasing the size of the memory map if needed
     * @param position
     * @param value
     * @throws IOException
     */
    @Override
    protected void setByte(long position, byte value) throws IOException {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);

        while(page_number >= intArrays.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, intArrays.size());

            intArrays.add(intArraysChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * intArrays.size(), PAGE_SIZE));
        }
        intArrays.get(page_number).put(page_position, value);
    }


    /**
     * Write the byte array given as argument to the byte buffer starting at the given position.
     *
     * @param position
     * @param array
     * @throws java.io.IOException
     */
    @Override
    protected void setBytes(long position, byte[] array) throws IOException {
        int page_position   = (int) (position % PAGE_SIZE);
        int page_number     = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + array.length) / PAGE_SIZE);

        while(end_page_number >= intArrays.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, intArrays.size());

            intArrays.add(intArraysChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * intArrays.size(), PAGE_SIZE));
        }

        if (page_number == end_page_number) {
            // efficient bulk put within page
            ((ByteBuffer)intArrays.get(page_number).position(page_position)).put(array);
        } else {
            // slower iteration over page boundaries
            for(int i=0; i<array.length; i++) {
                setByte(position + i, array[i]);
            }
        }
    }


    /**
     * Write the byte array contained in the given buffer into this byte array, starting at the given position
     * and reading until the buffer's limit is reached.
     *
     * @param position
     * @param buffer
     * @throws java.io.IOException
     */
    @Override
    protected void setBytesBuffer(long position, ByteBuffer buffer) throws IOException {
        int page_position   = (int) (position % PAGE_SIZE);
        int page_number     = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + buffer.limit()) / PAGE_SIZE);

        while(end_page_number >= intArrays.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, intArrays.size());

            intArrays.add(intArraysChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * intArrays.size(), PAGE_SIZE));
        }

        if (page_number == end_page_number) {
            // efficient bulk put within page
            ((ByteBuffer)intArrays.get(page_number).position(page_position)).put(buffer);
        } else {
            // slower iteration over page boundaries
            for(int i=0; i<buffer.limit(); i++) {
                setByte(position + i, buffer.get(i));
            }
        }
    }

    /**
     * Return the array at the given position.
     *
     * @param position
     * @return
     */
    public int[] getArray(long position) {
        // first two bytes represent capacity (ignored)
        // second two bytes represent length of array
        int length = getLength(position);

        // starting at position+2 we iterate until length and read integers
        int[] result = new int[length];

        for(int i=0; i < length; i++) {
            result[i] = readInt((position + 8) + (i*4));
        }

        return result;
    }

    private void setArray(long position, int[] values) throws IOException {
        if(values.length > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("maximum block size exceeded");
        }

        // first four bytes represent capacity
        int blocks_needed = (values.length / BLOCK_SIZE) + 1;
        int space_needed  = blocks_needed * BLOCK_SIZE;

        writeInt(position, space_needed);

        // second four bytes represent length of array
        writeInt(position + 4, values.length);

        // then we iterate over the array and store the values
        for(int i=0; i < values.length; i++) {
            writeInt((position + 8) + (i * 4), values[i]);
        }
    }

    /**
     * Free the array at the position given as argument and make its space available to other arrays.
     * This method will simply update the list of free areas maintained by the class.
     *
     * @param position
     * @throws IOException
     */
    private void freeArray(long position) throws IOException {
        // get the capacity of the array at the given position
        int capacity = getCapacity(position);

        // create-or-update the entry for this capacity in the free areas map
        if(!freeAreas.containsKey(capacity)) {
            freeAreas.put(capacity, new LinkedList<Long>());
        }
        freeAreas.get(capacity).add(position);
    }


    /**
     * Return the position of an array of at least minSize to use for a new array. If an appropriately sized
     * block can be found in freeAreas, it will be used; otherwise a new area will be allocated at the end of
     * the buffer.
     *
     * @param minSize
     * @return
     */
    private long allocateArray(int minSize) {
        // try finding an entry in the map of free areas that has an appropriate size
        Map.Entry<Integer,Queue<Long>> candidate = freeAreas.higherEntry(minSize);
        if(candidate != null && candidate.getValue().size() > 0) {
            return candidate.getValue().poll();
        }

        // else allocate a new area of appropriate size
        int blocks_needed = (minSize / BLOCK_SIZE) + 1;
        int space_needed  = blocks_needed * BLOCK_SIZE;

        long arrayPos = endPointer;
        endPointer += 8 + (space_needed * 4);
        return arrayPos;
    }

    /**
     * Update the array at the given position using the given values. If the new values do not fit
     * into the free space available in the block, the block is freed and the array is allocated in a new
     * free space. The new starting position is returned by the method.
     *
     * @param position
     * @param values
     * @return
     */
    public long updateArray(long position, int[] values) throws IOException {
        // check if the block is of appropriate capacity
        int capacity = getCapacity(position);
        if(values.length < capacity) {
            setArray(position,values);
            return position;
        } else {
            long newPosition = allocateArray(values.length);
            setArray(newPosition, values);
            freeArray(position);
            return newPosition;
        }
    }


    /**
     * Allocate a new block and store the given values in it. The starting position is returned by the method.
     *
     * @param values
     * @return
     * @throws IOException
     */
    public long newArray(int[] values) throws IOException {
        long newPosition = allocateArray(values.length);
        setArray(newPosition, values);
        return newPosition;
    }

    public long getEndPointer() {
        return endPointer;
    }

    // debugging
    public byte[] toByteArray() {
        if(endPointer < Integer.MAX_VALUE) {
            byte[] result = new byte[(int)endPointer];
            for(int i=0;i<endPointer; i++) {
                result[i] = getByte(i);
            }
            return result;
        } else {
            return null;
        }
    }


    public void copyTo(MappedIntArrays other) throws IOException {
        for(long i = 0; i < endPointer; i++) {
            other.setByte(i, this.getByte(i));
        }
        other.endPointer = this.endPointer;
        for(Map.Entry<Integer,Queue<Long>> entry : this.freeAreas.entrySet()) {
            other.freeAreas.put(entry.getKey(), new LinkedList<Long>(entry.getValue()));
        }
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("MappedIntArrays:\n");
        result.append(" - end pointer: ");
        result.append(getEndPointer());
        result.append("\n");
        if(freeAreas.size() > 0) {
            result.append(" - free fragments:\n");
            for(Map.Entry<Integer,Queue<Long>> entry : freeAreas.entrySet()) {
                result.append("   * size ");
                result.append(entry.getKey());
                result.append(": ");
                result.append(entry.getValue().size());
                result.append("\n");
            }
        } else {
            result.append(" - no free fragments\n");
        }
        return result.toString();
    }

    /**
     * Get the element with the given index of the array starting at the given position; no bounds checking is
     * performed, i.e. unexpected results will occur when the limit is exceeded.
     *
     * @param start
     * @param index
     * @return
     */
    public int get(long start, int index) {
        return readInt(start + 8 + index * 4);
    }

    /**
     * Search for the value passed as argument in the array starting at the given starting position
     * @param start
     * @param value
     * @return
     */
    public int binarySearch(long start, int value) {
        return binarySearch(start, value, getLength(start));
    }

    /**
     * Search for the value passed as argument in the array starting at the given starting position up to the
     * count. No boundary checking performed in case count exceeds the array length.
     *
     * @param start
     * @param value
     * @return
     */
    public int binarySearch(long start, int value, int count) {
        return binarySearchIndex(start + 8, Ints.toByteArray(value), 4, count, true);
    }


    public long insertAt(long start, int value, int position) throws IOException {
        int count    = getLength(start);
        int capacity = getCapacity(start);

        if(count + 1 < capacity) {
            // block insert, we move the remaining bytes after the position by 4 to the right and write the
            // value at the right position
            long elempos = start + 8 + 4 * position;
            int  remaining = 4 * (count - position);
            copyBytes(elempos, elempos + 4, remaining);
            writeInt(elempos, value);

            // update count
            setLength(start, count+1);

            return start;
        } else {
            int  newCount = count + 1;
            long newStart = allocateArray(newCount);

            // first four bytes represent capacity
            int blocks_needed = (newCount / BLOCK_SIZE) + 1;
            int space_needed  = blocks_needed * BLOCK_SIZE;

            writeInt(newStart, space_needed);

            // second four bytes represent length of array
            writeInt(newStart + 4, newCount);

            // then we block copy the values from the old block start to the new block start

            // before position
            copyBytes(start + 8, newStart + 8, position * 4);

            // position
            writeInt(newStart + 8 + position * 4, value);

            // after position
            copyBytes(start + 8 + position * 4, newStart + 8 + (position + 1) * 4, (count - position) * 4);

            freeArray(start);
            return newStart;

        }
    }


    protected long addInternal(long start, int value, int count, int capacity) throws IOException {
        if(count + 1 < capacity) {
            // block insert, we move the remaining bytes after the position by 4 to the right and write the
            // value at the right position
            long elempos = start + 8 + 4 * count;
            writeInt(elempos, value);

            // update count
            setLength(start, count+1);

            return start;
        } else {
            int  newCount = count + 1;
            long newStart = allocateArray(newCount);

            // first four bytes represent capacity
            int blocks_needed = (newCount / BLOCK_SIZE) + 1;
            int space_needed  = blocks_needed * BLOCK_SIZE;

            writeInt(newStart, space_needed);

            // second four bytes represent length of array
            setLength(newStart, newCount);

            // then we block copy the values from the old block start to the new block start

            // previous array
            copyBytes(start + 8, newStart + 8, count * 4);

            // new value at end
            writeInt(newStart + 8 + count * 4, value);

            freeArray(start);

            return newStart;
        }

    }


    public long add(long start, int value) throws IOException {
        return addInternal(start,value,getLength(start), getCapacity(start));
    }


    /**
     * Add the value to the end of the array if it does not exist yet.
     *
     * @param start starting position of array
     * @param value value to add
     * @return starting position of (possibly shifted) array
     * @throws IOException
     */
    public long addSet(long start, int value) throws IOException {
        int count    = getLength(start);

        for(int i = 0; i<count; i++) {
            if(value == get(start, i)) {
                return start;
            }
        }

        return addInternal(start,value,count, getCapacity(start));
    }
}
