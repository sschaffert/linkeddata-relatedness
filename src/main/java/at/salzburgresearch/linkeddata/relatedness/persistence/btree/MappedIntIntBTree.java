package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.base.Function;
import com.google.common.primitives.Ints;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntIntBTree extends MappedBTree implements Iterable<MappedIntIntBTree.KeyValuePair> {

    public static final int NaN = Integer.MIN_VALUE;

    private static final Function<byte[],String> keyPrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return String.format("%d", MappedBase.makeInt(input));
        }
    };


    private static final Function<byte[], String> valuePrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return String.format("'%d'", MappedBase.makeInt(input));
        }
    };


    public MappedIntIntBTree(File directory, String basename, int nodeSize) throws IOException {
        super(directory, basename, nodeSize, 4, 4);
    }

    /**
     * Return a serializer function suitable for creating human-readable representations of keys
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getKeySerializer() {
        return keyPrinter;
    }

    /**
     * Return a serializer function suitable for creating human-readable representations of values
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getValueSerializer() {
        return valuePrinter;
    }

    public boolean contains(int key) {
        return getInternal(Ints.toByteArray(key)) != -1;
    }


    public int get(int key) {
        long position = getInternal(Ints.toByteArray(key));

        if(position != -1) {
            return readInt(position);
        } else {
            return NaN;
        }
    }

    public int put(int key, int value) throws IOException {
        byte[] oldval = putInternal(Ints.toByteArray(key), Ints.toByteArray(value));
        if(oldval != null) {
            return makeInt(oldval);
        } else {
            return NaN;
        }
    }


    public Iterator<KeyValuePair> iterator() {
        return collectBFS(new CollectorFunction<KeyValuePair>() {
            @Override
            public KeyValuePair collect(byte[] key, byte[] value, long node, int index, long left, long right) {
                return new KeyValuePair(makeInt(key), makeInt(value));
            }
        });
    }


    public static class KeyValuePair {
        public int key;
        public int value;

        public KeyValuePair(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public final boolean isNaN(int value) {
        return NaN == value;
    }

}
