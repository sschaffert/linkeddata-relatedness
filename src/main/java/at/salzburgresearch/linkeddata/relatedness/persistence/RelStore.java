package at.salzburgresearch.linkeddata.relatedness.persistence;

import at.salzburgresearch.linkeddata.relatedness.model.Relation;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface RelStore {

    /**
     * Return the Node ID for the URI node passed as argument. The implementation can decide how this value
     * is computed.
     *
     * @param uri  URI of the node
     * @return     an (pseudo-unique) integer value representing the node ID
     */
    int getNodeID(String uri);


    /**
     * Return the relation ID for the two URI nodes passed as argument. Needs to be commutative. Implementations
     * can decide on how this is implemented concretely.
     *
     * @param nodeIdX
     * @param nodeIdY
     * @return
     */
    int getRelationID(int nodeIdX, int nodeIdY);


    void addRelatedness(Relation r);

    void setRelatedness(String uri1, String uri2, double relatedness);

    void setRelatedness(int nodeIdX, int nodeIdY, double relatedness);

    /**
     * Get the relatedness value between the two uris passed as argument, if it exists. Returns 0.0 otherwise.
     * @param uriX
     * @param uriY
     * @return
     */
    double getRelatedness(String uriX, String uriY);


    /**
     * Get the relatedness value between the node ids of the two uris passed as argument, if it exists. Returns
     * 0.0 otherwise.
     * @param nodeIdX node ID of the first node
     * @param nodeIdY node ID of the second node
     * @return
     */
    double getRelatedness(int nodeIdX, int nodeIdY);


    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     * @param uri
     * @return
     */
    int[] getAdjacent(String uri);

    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     * @param nodeId
     * @return
     */
    int[] getAdjacent(int nodeId);


    boolean isEmpty();

    /**
     * Copy all contents of this relatedness DB to the other relatedness DB.
     * @param other
     */
    void copyTo(RelStore other);

    /**
     * Export to the disambiguation database using the MapDB given as argument.
     * @param mapdb
     */
    void exportTo(File mapdb);

    /**
     * Export to the disambiguation database using the Java Map given as argument.
     * @param relmap
     */
    void exportTo(Map<Integer, Double> relmap);

    public Iterator<? extends Adjacent> getNodes();

    /**
     * Iterator representation of a node and its adjacent nodes
     */
    public static class Adjacent {
        protected int node;
        protected int[] adjacent;

        public Adjacent(int node, int[] adjacent) {
            this.node = node;
            this.adjacent = adjacent;
        }

        public int[] getAdjacent() {
            return adjacent;
        }

        public int getNode() {
            return node;
        }
    }

}
