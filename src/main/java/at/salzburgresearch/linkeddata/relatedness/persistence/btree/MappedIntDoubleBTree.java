package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.base.Function;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntDoubleBTree extends MappedBTree implements Iterable<MappedIntDoubleBTree.KeyValuePair> {

    private static final Function<byte[],String> keyPrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return "" + MappedBase.makeInt(input);
        }
    };


    private static final Function<byte[], String> valuePrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return String.format("'%.4f'", makeDouble(input));
        }
    };



    public MappedIntDoubleBTree(File directory, String basename, int nodeSize) throws IOException {
        super(directory, basename, nodeSize, 4, 8);
    }


    public boolean contains(int key) {
        return getInternal(Ints.toByteArray(key)) != -1;
    }


    public double get(int key) {
        long position = getInternal(Ints.toByteArray(key));

        if(position != -1) {
            return readDouble(position);
        } else {
            return Double.NaN;
        }
    }

    public double put(int key, double value) throws IOException {
        byte[] oldval = putInternal(Ints.toByteArray(key), Longs.toByteArray(Double.doubleToLongBits(value)));
        if(oldval != null) {
            return makeDouble(oldval);
        } else {
            return Double.NaN;
        }
    }


    /**
     * Return a serializer function suitable for creating human-readable representations of keys
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getKeySerializer() {
        return keyPrinter;
    }

    /**
     * Return a serializer function suitable for creating human-readable representations of values
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getValueSerializer() {
        return valuePrinter;
    }

    public Iterator<KeyValuePair> iterator() {
        return collectBFS(new CollectorFunction<KeyValuePair>() {
            @Override
            public KeyValuePair collect(byte[] key, byte[] value, long node, int index, long left, long right) {
                return new KeyValuePair(makeInt(key), makeDouble(value));
            }
        });
    }


    public static class KeyValuePair {
        public int key;
        public double value;

        public KeyValuePair(int key, double value) {
            this.key = key;
            this.value = value;
        }
    }

    public final boolean isNaN(double value) {
        return Double.isNaN(value);
    }
}
