package at.salzburgresearch.linkeddata.relatedness.persistence;

import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedBTree;
import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedIntArrays;
import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedIntDoubleBTree;
import at.salzburgresearch.linkeddata.relatedness.persistence.btree.MappedIntLongBTree;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * An optimization of a relation store using a custom b-tree implementation and memory-mapped files. The implementation
 * creates three files in the work directory:
 * - a mapping from int -> double values for representing the relatedness between two nodes
 * - a mapping from int -> int for mapping from a node to the starting position of its adjacency array
 * - a mapping for representing adjacency arrays (array of integers for each node)
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelStoreBTree extends RelStoreBase implements RelStore {

    private static Logger log = LoggerFactory.getLogger(RelStoreBTree.class);

    /**
     * Stores mappings of the form (int,double) where the int value is the key in the b-tree
     */
    private MappedIntDoubleBTree relIndex;

    /**
     * Stores mappings of the form (int,int) where the first argument is the ID of the node, and the
     * second argument is the pointer to the adjacency value array in adjValues
     */
    private MappedIntLongBTree adjIndex;

    /**
     * Stores arrays of adjacency values; structure is:
     * - two bytes for representing the size S of the int array to come
     * - S * 4 bytes for holding the int array of size S
     * initial size for each array is 32
     */
    private MappedIntArrays adjValues;

    public RelStoreBTree(File directory, String prefix) throws IOException {
        relIndex = new MappedIntDoubleBTree(directory, prefix + "-relations", 16);
        adjIndex = new MappedIntLongBTree(directory, prefix + "-adjacency", 16);
        adjValues = new MappedIntArrays(directory, prefix + "-adjacency");
    }


    @Override
    protected Iterator<? extends HashPair> getRelatednessValues() {
        return relIndex.collectBFS(new MappedBTree.CollectorFunction<HashPair>() {
            @Override
            public HashPair collect(byte[] key, byte[] value, long node, int index, long left, long right) {
                return new HashPair(Ints.fromByteArray(key), Double.longBitsToDouble(Longs.fromByteArray(value)));
            }
        });
    }

    @Override
    public Iterator<? extends Adjacent> getNodes() {
        return adjIndex.collectBFS(new MappedBTree.CollectorFunction<Adjacent>() {
            @Override
            public Adjacent collect(byte[] key, byte[] value, long node, int index, long left, long right) {
                return new Adjacent(Ints.fromByteArray(key), getAdjacent(Ints.fromByteArray(key)));
            }
        });
    }

    @Override
    public void setRelatedness(int nodeIdX, int nodeIdY, double relatedness) {
        try {
            double oldval = relIndex.put(getRelationID(nodeIdX,nodeIdY), relatedness);
            if(relIndex.isNaN(oldval)) {
                // update adjacency index
                updateAdjacency(nodeIdX,nodeIdY);
                updateAdjacency(nodeIdY, nodeIdX);
            }
        } catch (IOException e) {
            log.error("error updating relatedness value", e);
        }
    }

    /**
     * Get the relatedness value between the node ids of the two uris passed as argument, if it exists. Returns
     * 0.0 otherwise.
     *
     * @param nodeIdX node ID of the first node
     * @param nodeIdY node ID of the second node
     * @return
     */
    @Override
    public double getRelatedness(int nodeIdX, int nodeIdY) {
        double value = relIndex.get(getRelationID(nodeIdX,nodeIdY));
        if(relIndex.isNaN(value)) {
            return 0.0;
        } else {
            return value;
        }
    }


    protected void updateAdjacency(int nodeIdX, int nodeIdY) {
        try {
            long pos = adjIndex.get(nodeIdX);

            if(!adjIndex.isNaN(pos)) {
                /*
                long newPos = adjValues.addSet(pos, nodeIdY);
                if(newPos != pos) {
                    adjIndex.put(nodeIdX, newPos);
                }
                */
                int idx = adjValues.binarySearch(pos, nodeIdY);
                if(idx < 0) {
                    int posY = - idx - 1;
                    // insert nodeY at posY
                    long newPos = adjValues.insertAt(pos,nodeIdY, posY);

                    if(newPos != pos) {
                        adjIndex.put(nodeIdX, newPos);
                    }
                }
            } else {
                pos = adjValues.newArray(new int[]{nodeIdY});
                adjIndex.put(nodeIdX, pos);
            }
        } catch (IOException ex) {
            log.error("error while updating adjacency matrix",ex);
        }
    }


    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     *
     * @param nodeId
     * @return
     */
    @Override
    public int[] getAdjacent(int nodeId) {
        long pos = adjIndex.get(nodeId);
        if(!adjIndex.isNaN(pos)) {
            return adjValues.getArray(pos);
        } else {
            return new int[0];
        }
    }

    @Override
    public boolean isEmpty() {
        return relIndex.isEmpty();
    }

    /**
     * Copy all contents of this relatedness DB to the other relatedness DB.
     *
     * @param other
     */
    @Override
    public void copyTo(RelStore other) {
        try {
            if(other instanceof RelStoreBTree) {
                RelStoreBTree rb = (RelStoreBTree)other;
                this.relIndex.copyTo(rb.relIndex);
                this.adjIndex.copyTo(rb.adjIndex);
                this.adjValues.copyTo(rb.adjValues);
            } else {
                throw new IllegalArgumentException("cannot copy between different kinds of relatedness databases");
            }
        } catch (IOException ex) {
            throw new IllegalStateException("copying failed (I/O exception when accessing memory-mapped files)",ex);
        }

    }
}
