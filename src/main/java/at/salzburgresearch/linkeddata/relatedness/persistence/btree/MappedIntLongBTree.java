package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.base.Function;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MappedIntLongBTree extends MappedBTree implements Iterable<MappedIntLongBTree.KeyValuePair> {

    public static final long NaN = Long.MIN_VALUE;

    private static final Function<byte[],String> keyPrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return String.format("%d", MappedBase.makeInt(input));
        }
    };


    private static final Function<byte[], String> valuePrinter = new Function<byte[], String>() {
        @Override
        public String apply(byte[] input) {
            return String.format("'%d'", MappedBase.makeInt(input));
        }
    };


    public MappedIntLongBTree(File directory, String basename, int nodeSize) throws IOException {
        super(directory, basename, nodeSize, 4, 8);
    }

    /**
     * Return a serializer function suitable for creating human-readable representations of keys
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getKeySerializer() {
        return keyPrinter;
    }

    /**
     * Return a serializer function suitable for creating human-readable representations of values
     *
     * @return
     */
    @Override
    protected Function<byte[], String> getValueSerializer() {
        return valuePrinter;
    }


    public boolean contains(int key) {
        return getInternal(Ints.toByteArray(key)) != -1;
    }


    public long get(int key) {
        long position = getInternal(Ints.toByteArray(key));

        if(position != -1) {
            return readLong(position);
        } else {
            return NaN;
        }
    }

    public long put(int key, long value) throws IOException {
        byte[] oldval = putInternal(Ints.toByteArray(key), Longs.toByteArray(value));
        if(oldval != null) {
            return makeLong(oldval);
        } else {
            return NaN;
        }
    }

    public Iterator<KeyValuePair> iterator() {
        return collectBFS(new CollectorFunction<KeyValuePair>() {
            @Override
            public KeyValuePair collect(byte[] key, byte[] value, long node, int index, long left, long right) {
                return new KeyValuePair(makeInt(key), makeLong(value));
            }
        });
    }


    public static class KeyValuePair {
        public int key;
        public long value;

        public KeyValuePair(int key, long value) {
            this.key = key;
            this.value = value;
        }
    }

    public boolean isNaN(long value) {
        return NaN == value;
    }

}
