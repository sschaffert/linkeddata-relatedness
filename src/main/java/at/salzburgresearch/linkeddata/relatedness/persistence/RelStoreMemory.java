package at.salzburgresearch.linkeddata.relatedness.persistence;

import com.google.common.collect.Iterators;
import com.google.common.primitives.Ints;
import gnu.trove.iterator.TIntDoubleIterator;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntDoubleMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.util.Arrays;
import java.util.Iterator;

/**
 * In-memory implementation of a RelatednessDB using HashMaps
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelStoreMemory extends RelStoreBase implements RelStore {

    private TIntDoubleMap mainMap;
    private TIntObjectMap<int[]> adjMap;


    public RelStoreMemory() {
        this.mainMap = new TIntDoubleHashMap();
        this.adjMap = new TIntObjectHashMap<int[]>();
    }


    protected void updateAdjacency(int nodeIdX, int nodeIdY) {
        if(adjMap.containsKey(nodeIdX)) {
            int[] adjXOld = adjMap.get(nodeIdX);
            int idx = Arrays.binarySearch(adjXOld, nodeIdY);
            if(idx < 0) {
                int[] adjXNew = new int[adjXOld.length + 1];
                int posY = - idx - 1;
                // copy over until posY
                System.arraycopy(adjXOld,0,adjXNew,0,posY);
                adjXNew[posY] = nodeIdY;
                System.arraycopy(adjXOld, posY, adjXNew, posY + 1, adjXOld.length - posY);
                adjMap.put(nodeIdX, adjXNew);
            }
        } else {
            adjMap.put(nodeIdX, new int[]{nodeIdY});
        }
    }

    @Override
    public boolean isEmpty() {
        return mainMap.isEmpty();
    }


    @Override
    protected Iterator<HashPair> getRelatednessValues() {
        final TIntDoubleIterator wrapped = mainMap.iterator();
        return new Iterator<HashPair>() {
            @Override
            public boolean hasNext() {
                return wrapped.hasNext();
            }

            @Override
            public HashPair next() {
                wrapped.advance();
                return new HashPair(wrapped.key(), wrapped.value());
            }

            @Override
            public void remove() {
                wrapped.remove();
            }
        };
    }

    @Override
    public Iterator<? extends Adjacent> getNodes() {
        final TIntObjectIterator wrapped = adjMap.iterator();
        return new Iterator<Adjacent>() {
            @Override
            public boolean hasNext() {
                return wrapped.hasNext();
            }

            @Override
            public Adjacent next() {
                wrapped.advance();
                return new Adjacent(wrapped.key(), (int[])wrapped.value());
            }

            @Override
            public void remove() {
                wrapped.remove();
            }
        };
    }

    @Override
    public void setRelatedness(int nodeIdX, int nodeIdY, double relatedness) {
        if(mainMap.put(getRelationID(nodeIdX,nodeIdY), relatedness) == mainMap.getNoEntryValue()) {

            // update adjacency index
            updateAdjacency(nodeIdX,nodeIdY);
            updateAdjacency(nodeIdY,nodeIdX);
        }
    }


    /**
     * Get the relatedness value between the node ids of the two uris passed as argument, if it exists. Returns
     * 0.0 otherwise.
     *
     * @param nodeIdX node ID of the first node
     * @param nodeIdY node ID of the second node
     * @return
     */
    @Override
    public double getRelatedness(int nodeIdX, int nodeIdY) {
        if(mainMap.containsKey(getRelationID(nodeIdX,nodeIdY))) {
            return mainMap.get(getRelationID(nodeIdX,nodeIdY));
        } else {
            return 0.0;
        }
    }

    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     *
     * @param nodeId
     * @return
     */
    @Override
    public int[] getAdjacent(int nodeId) {
        if(adjMap.containsKey(nodeId)) {
            return adjMap.get(nodeId);
        } else {
            return new int[0];
        }
    }

    /**
     * Copy all contents of this relatedness DB to the other relatedness DB.
     *
     * @param other
     */
    @Override
    public void copyTo(RelStore other) {
        if(other instanceof RelStoreMemory) {
            RelStoreMemory omem = (RelStoreMemory)other;

            TIntDoubleIterator it1 = mainMap.iterator();
            while(it1.hasNext()) {
                it1.advance();
                omem.mainMap.put(it1.key(), it1.value());
            }

            TIntObjectIterator it2 = adjMap.iterator();
            while(it2.hasNext()) {
                it2.advance();
                int[] data = (int[])it2.value();
                omem.adjMap.put(it2.key(), data);
            }

        } else {
            throw new IllegalArgumentException("cannot copy between different kinds of relatedness databases");
        }
    }
}
