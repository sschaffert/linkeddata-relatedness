package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Some convenience functions useful for any kind of mapped byte buffer, subclasses only need to provide
 * setByte and getByte methods
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class MappedBase {

    protected static final int makeInt(byte[] array) {
        return Ints.fromByteArray(array);
    }

    protected static final long makeLong(byte[] a) {
        return Longs.fromByteArray(a);
    }

    protected static final double makeDouble(byte[] array) {
        return Double.longBitsToDouble(makeLong(array));
    }

    public static final int compareArraysUnsigned(byte[] left, byte[] right) {
        if(left.length != right.length) {
            throw new IllegalArgumentException("left and right arguments have different lenght");
        }
        for(int i=0; i<left.length; i++) {
            if(left[i] < right[i]) {
                return -1;
            } else if(left[i] > right[i]) {
                return +1;
            }
        }
        return 0;
    }


    protected final int compareArraysUnsigned(long leftPos, byte[] right) {
        for(int i=0; i<right.length; i++) {
            byte left_i = getByte(leftPos + i);
            if(left_i < right[i]) {
                return -1;
            } else if(left_i > right[i]) {
                return +1;
            }
        }
        return 0;
    }


    public static final int compareArraysSigned(byte[] left, byte[] right) {
        if(left.length != right.length) {
            throw new IllegalArgumentException("left and right arguments have different lenght");
        }

        // special treatment of the first byte, as it contains the sign
        boolean leftNeg = (left[0] & 0xF0) > 0;
        boolean rightNeg = (right[0] & 0xF0) > 0;
        if(leftNeg && !rightNeg) {
            return -1;
        }  else if(rightNeg && !leftNeg) {
            return +1;
        } else  {
            if( (left[0] & 0x0F) < (right[0] & 0x0F)) {
                return leftNeg ? +1 : -1;
            } else if((left[0] & 0x0F) > (right[0] & 0x0F) ) {
                return leftNeg ? -1 : +1;
            }
        }

        for(int i=1; i<left.length; i++) {
            if(left[i] < right[i]) {
                return -1;
            } else if(left[i] > right[i]) {
                return +1;
            }
        }
        return 0;
    }


    protected final int compareArraysSigned(long leftPos, byte[] right) {

        byte left_0 = getByte(leftPos);

        // special treatment of the first byte, as it contains the sign
        boolean leftNeg = (left_0 & 0xF0) > 0;
        boolean rightNeg = (right[0] & 0xF0) > 0;
        if(leftNeg && !rightNeg) {
            return -1;
        }  else if(rightNeg && !leftNeg) {
            return +1;
        } else  {
            if( (left_0 & 0x0F) < (right[0] & 0x0F)) {
                return leftNeg ? +1 : -1;
            } else if((left_0 & 0x0F) > (right[0] & 0x0F) ) {
                return leftNeg ? -1 : +1;
            }
        }

        for(int i=1; i<right.length; i++) {
            byte left_i = getByte(leftPos + i);
            if(left_i < right[i]) {
                return -1;
            } else if(left_i > right[i]) {
                return +1;
            }
        }
        return 0;
    }


    /**
     * Retrieve the byte at the given position in the buffer. A helper method to retrieve bytes across the page size
     *
     * @param position
     * @return
     */
    protected abstract byte getByte(long position);

    /**
     * Set the byte at the given position to the given value, increasing the size of the memory map if needed
     * @param position
     * @param value
     * @throws java.io.IOException
     */
    protected abstract void setByte(long position, byte value) throws IOException;


    /**
     * Get the byte array starting at the given position (read-only). The byte array is guaranteed to allow reading
     * at least the specified number of bytes.
     *
     * @param position
     * @return
     * @throws IOException
     */
    protected abstract byte[] getBytes(long position, int length);


    /**
     * Return a ByteBuffer view of the data starting at the given position until the given length.
     *
     * @param position
     * @return
     */
    protected abstract ByteBuffer getBytesBuffer(long position, int length);

    /**
     * Write the byte array given as argument to the byte buffer starting at the given position.
     *
     * @param position
     * @param array
     * @throws IOException
     */
    protected abstract void setBytes(long position, byte[] array) throws IOException;


    /**
     * Write the byte array contained in the given buffer into this byte array, starting at the given position
     * and reading until the buffer's limit is reached.
     *
     * @param position
     * @param buffer
     * @throws IOException
     */
    protected abstract void setBytesBuffer(long position, ByteBuffer buffer) throws IOException;

    /**
     * Return the pointer pointing to the end of the buffer (i.e. the element after the last element of the buffer)
     * @return
     */
    protected abstract long getEndPointer();

    protected final int readInt(long pos)  {
        return getBytesBuffer(pos,4).getInt();
        //return makeInt(getBytes(pos, 4));
    }

    protected final void writeInt(long pos, int value) throws IOException {
        setBytes(pos, Ints.toByteArray(value));
    }

    protected final long readLong(long pos)  {
        return getBytesBuffer(pos,8).getLong();
        //return makeLong(getBytes(pos, 8));
    }

    protected final void writeLong(long pos, long value) throws IOException {
        setBytes(pos, Longs.toByteArray(value));
    }

    protected final double readDouble(long pos)  {
        return Double.longBitsToDouble(readLong(pos));
    }

    protected final void writeDouble(long pos, double value) throws IOException {
        writeLong(pos, Double.doubleToLongBits(value));
    }

    protected void copyBytes(long oldpos, long newpos, int count) throws IOException {
        if(newpos > oldpos) {
            setBytesBuffer(newpos, getBytesBuffer(oldpos,count));
        } else {
            setBytes(newpos, getBytes(oldpos, count));
        }
    }

    /**
     * Perform a binary search over a segment of the buffer starting at "position" for the byte sequence passed over
     * as "value". Each entry is considered to be "step" bytes long (step must be at least value.length but can be more).
     * The value of "count" is the highest index of a value +1. If "signed" is true, the value is considered signed
     * (i.e. most significant bit carries sign).
     *
     * @param start
     * @param value
     * @param step
     * @param count
     * @param signed
     * @return
     */


    protected int binarySearchIndex(long start, byte[] value, int step, int count, boolean signed) {
        int lower = 0, middle = 0, upper = count - 1;

        int _cmp = 0;

        long _key = -1;

        while (lower <= upper ) {
            middle = (upper + lower) >>> 1;

            _key   = start + middle * step;

            if(signed) {
                _cmp   = compareArraysSigned(_key, value);
            } else {
                _cmp   = compareArraysUnsigned(_key, value);
            }
            if(_cmp == 0) {
                return middle;
            } else if(_cmp < 0) {
                lower = middle + 1;
            } else {
                upper = middle - 1;
            }

        }

        return - (lower + 1);

    }
}
