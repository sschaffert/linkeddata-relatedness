package at.salzburgresearch.linkeddata.relatedness.persistence;

import at.salzburgresearch.linkeddata.relatedness.model.Relation;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.io.File;
import java.text.Collator;
import java.util.Iterator;
import java.util.Map;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class RelStoreBase implements RelStore {

    protected static final Collator collator = Collator.getInstance();

    protected HashFunction hasher;

    protected RelStoreBase() {
        this.hasher = Hashing.goodFastHash(64);
    }

    /**
     * Return the Node ID for the URI node passed as argument. The implementation can decide how this value
     * is computed.
     *
     * @param uri URI of the node
     * @return an (pseudo-unique) integer value representing the node ID
     */
    @Override
    public int getNodeID(String uri) {
        return uri.hashCode();
    }

    /**
     * Return the relation ID for the two URI nodes passed as argument. Needs to be commutative. Implementations
     * can decide on how this is implemented concretely.
     *
     * @param nodeIdX
     * @param nodeIdY
     * @return
     */
    @Override
    public int getRelationID(int nodeIdX, int nodeIdY) {
        return nodeIdX * nodeIdY;
    }



    @Override
    public void addRelatedness(Relation r) {
        setRelatedness(r.getConceptUri1(), r.getConceptUri2(), r.getRelatedness());
    }

    @Override
    public void setRelatedness(String uri1, String uri2, double relatedness) {
        setRelatedness(getNodeID(uri1), getNodeID(uri2), relatedness);
    }


    /**
     * Get the relatedness value between the two uris passed as argument, if it exists. Returns 0.0 otherwise.
     *
     * @param uriX
     * @param uriY
     * @return
     */
    @Override
    public double getRelatedness(String uriX, String uriY) {
        return getRelatedness(getNodeID(uriX), getNodeID(uriY));
    }


    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     *
     * @param uri
     * @return
     */
    @Override
    public int[] getAdjacent(String uri) {
        return getAdjacent(getNodeID(uri));
    }

    /**
     * Export to the disambiguation database using the MapDB given as argument.
     * @param mapdb
     */
    @Override
    public void exportTo(File mapdb) {
        DB reldb = DBMaker
                .newFileDB(mapdb)
                .closeOnJvmShutdown()
                .compressionEnable()
                .transactionDisable()
                .make();

        try {
            Map<Integer,Double> relmap = reldb.getTreeMap("relatedness");

            exportTo(relmap);

            //reldb.commit();
            reldb.compact();
        } finally {
            reldb.close();
        }
    }


    /**
     * Export to the disambiguation database using the Java Map given as argument.
     * @param relmap
     */
    @Override
    public void exportTo(Map<Integer, Double> relmap) {
        Iterator<? extends HashPair> it = getRelatednessValues();
        while(it.hasNext()) {
            HashPair rel= it.next();
            relmap.put(rel.hash, rel.relatedness);
        }
    }

    protected abstract Iterator<? extends HashPair> getRelatednessValues();

    protected static class HashPair {
        protected int hash;
        protected double relatedness;

        public HashPair(int hash, double relatedness) {
            this.hash = hash;
            this.relatedness = relatedness;
        }
    }

}
