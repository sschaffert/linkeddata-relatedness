package at.salzburgresearch.linkeddata.relatedness.persistence.btree;

import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Base class for implementing a btree in a memory-mapped buffer. Aims to be a highly scalable index implementation
 * for simple indexes. Structure of the memory map is very simple and may contain empty nodes:
 * 1 - 21 - 22 - 23 - ... - 31 - 32 - 33 .... etc
 * The byte representation of each node is built up as follows:
 * leftmost link - key - value - link - key - value - link - ... rightmost link - node size (4 byte)
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public abstract class MappedBTree extends MappedBase {

    private static Logger log = LoggerFactory.getLogger(MappedBTree.class);

    private static final long PAGE_SIZE = Integer.MAX_VALUE;  // size of a memory-mapped region

    private List<MappedByteBuffer> indexPages;

    private FileChannel indexChannel;

    /**
     * node size in number of entries
     */
    protected final int nodeSize;

    /**
     * key size in number of bytes
     */
    protected final int keySize;

    /**
     * value size in number of bytes
     */
    protected final int valueSize;

    protected final int totalNodeSize;

    protected final static int linkSize = 8;

    protected int maxLevel;


    protected long endPointer = -1;

    protected long rootPointer = -1;


    protected MappedBTree(File directory, String basename, int nodeSize, int keySize, int valueSize) throws IOException {
        if(!directory.exists() || !directory.isDirectory()) {
            throw new IllegalArgumentException("the directory "+directory+" does not exist or is not a directory!");
        } else {
            log.info("creating new memory-mapped file {}/{}.idx", directory, basename);
        }

        indexChannel = FileChannel.open(new File(directory, basename + ".idx").toPath(), StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW, StandardOpenOption.SPARSE, StandardOpenOption.DELETE_ON_CLOSE);

        indexPages = new ArrayList<>();

        // initialise the first page immediately
        indexPages.add(indexChannel.map(FileChannel.MapMode.READ_WRITE, 0, PAGE_SIZE));

        this.nodeSize  = nodeSize;
        this.keySize   = keySize;
        this.valueSize = valueSize;

        // bytes: nodeSize times the size of key and value, nodeSize+1 times the size of links, 4 bytes for the current node size
        this.totalNodeSize = nodeSize * (keySize + valueSize) + (nodeSize + 1) * linkSize + 4;
        this.maxLevel = 1;

        setEndPointer(16);
        setRootPointer(16);

        // init root node
        createRootNode();
    }


    /**
     * Calculate the total size of a node in number of bytes. The size is calculated taking into account:
     * <ul>
     *     <li>the number of entries per node (nodeSize)</li>
     *     <li>the size of the key</li>
     *     <li>the size of the value</li>
     *     <li>the size of the pointers (long) to leaf nodes (8)</li>
     *     <li>the size of the field for holding the current node size (4)</li>
     * </ul>
     * @return
     */
    public int getTotalNodeSize() {
        return  totalNodeSize;
    }

    /**
     * Return the position of the current root node of the b-tree (as a pointer in the byte array).
     *
     * @return
     */
    public long getRootPointer() {
        if(rootPointer < 0) {
            rootPointer = readLong(0);
        }
        return rootPointer;
    }

    /**
     * Update the position of the root node of the b-tree (pointer in the byte array).
     *
     * @param pos
     * @throws IOException
     */
    private void setRootPointer(long pos) throws IOException {
        writeLong(0, pos);
        rootPointer = pos;
    }

    /**
     * Get the position of the end of the b-tree in tbe byte array.
     *
     * @return
     */
    @Override
    public long getEndPointer() {
        if(endPointer < 0) {
            endPointer = readLong(8);
        }
        return endPointer;
    }

    /**
     * Update the position of the end of the b-tree in the byte array
     *
     * @param pos
     * @throws IOException
     */
    private void setEndPointer(long pos) throws IOException {
        writeLong(8, pos);
        endPointer = pos;
    }

    /**
     * Return the maximum level of the tree (runtime only, might yield wrong results)
     *
     * @return
     */
    public int getMaxLevel() {
        return maxLevel;
    }

    /**
     * Return the total number of nodes in this b-tree
     *
     * @return
     */
    public int getNodeCount() {
        return (int) ((getEndPointer() - 16) / totalNodeSize);
    }

    /**
     * Return the complete node at the given position as byte array (for debugging).
     *
     * @param pos
     * @return
     */
    public byte[] getNode(int pos) {
        return getBytes((long) pos, totalNodeSize);
    }

    /**
     * A helper method to retrieve bytes across the page size
     * @param position
     * @return
     */
    protected byte getByte(long position) {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);

        if(page_number > indexPages.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else {
            return indexPages.get(page_number).get(page_position);
        }
    }

    /**
     * Get the byte array starting at the given position (read-only). The byte array is guaranteed to allow reading
     * at least the specified number of bytes.
     *
     * @param position
     * @return
     * @throws java.io.IOException
     */
    @Override
    protected byte[] getBytes(long position, int length) {
        byte[] result = new byte[length];

        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + length) / PAGE_SIZE);

        if(end_page_number > indexPages.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else if (page_number == end_page_number) {
            // efficient bulk get within page
            ((ByteBuffer)indexPages.get(page_number).position(page_position)).get(result,0,length);
            return result;
        } else {
            log.warn("slow iteration over page boundaries!");

            // slower iteration over page boundaries
            for(int i=0; i<length; i++) {
                result[i] = getByte(position + i);
            }
            return result;
        }
    }

    /**
     * Return a ByteBuffer view of the data starting at the given position until the given length.
     *
     * @param position
     * @return
     */
    @Override
    protected ByteBuffer getBytesBuffer(long position, int length) {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + length) / PAGE_SIZE);

        if(end_page_number > indexPages.size()) {
            throw new IndexOutOfBoundsException("page "+page_number+" does not exist");
        } else if (page_number == end_page_number) {
            // efficient bulk get within page
            ByteBuffer result = ((ByteBuffer)indexPages.get(page_number).position(page_position)).slice();
            result.limit(length);
            return result;
        } else {
            ByteBuffer result = ByteBuffer.allocate(length);
            // slower iteration over page boundaries
            for(int i=0; i<length; i++) {
                result.put(getByte(position + i));
            }
            result.position(0);
            result.limit(length);
            return result;
        }
    }


    /**
     * Set the byte at the given position to the given value, increasing the size of the memory map if needed
     * @param position
     * @param value
     * @throws java.io.IOException
     */
    protected void setByte(long position, byte value) throws IOException {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);

        while(page_number >= indexPages.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, indexPages.size());

            indexPages.add(indexChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * indexPages.size(), PAGE_SIZE));
        }
        indexPages.get(page_number).put(page_position, value);
    }


    /**
     * Write the byte array given as argument to the byte buffer starting at the given position.
     *
     * @param position
     * @param array
     * @throws java.io.IOException
     */
    @Override
    protected void setBytes(long position, byte[] array) throws IOException {
        int page_position = (int) (position % PAGE_SIZE);
        int page_number   = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + array.length) / PAGE_SIZE);

        while(end_page_number >= indexPages.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, indexPages.size());

            indexPages.add(indexChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * indexPages.size(), PAGE_SIZE));
        }

        if (page_number == end_page_number) {
            // efficient bulk put within page
            ((ByteBuffer)indexPages.get(page_number).position(page_position)).put(array);
        } else {
            // slower iteration over page boundaries
            for(int i=0; i<array.length; i++) {
                setByte(position + i, array[i]);
            }
        }
    }

    /**
     * Write the byte array contained in the given buffer into this byte array, starting at the given position
     * and reading until the buffer's limit is reached.
     *
     * @param position
     * @param buffer
     * @throws java.io.IOException
     */
    @Override
    protected void setBytesBuffer(long position, ByteBuffer buffer) throws IOException {
        int page_position   = (int) (position % PAGE_SIZE);
        int page_number     = (int) (position / PAGE_SIZE);
        int end_page_number = (int) ( (position + buffer.limit()) / PAGE_SIZE);

        while(end_page_number >= indexPages.size()) {
            log.info("creating new page because of write request into unallocated region (requested page number: {}, actual page count: {})", page_number, indexPages.size());

            indexPages.add(indexChannel.map(FileChannel.MapMode.READ_WRITE, PAGE_SIZE * indexPages.size(), PAGE_SIZE));
        }

        if (page_number == end_page_number) {
            // efficient bulk put within page
            ((ByteBuffer)indexPages.get(page_number).position(page_position)).put(buffer);
        } else {
            // slower iteration over page boundaries
            for(int i=0; i<buffer.limit(); i++) {
                setByte(position + i, buffer.get(i));
            }
        }
    }


    /**
     * Return the key with the given index in the node starting at the given node position.
     *
     * @param nodepos
     * @param index
     * @return
     */
    private byte[] getKeyAt(long nodepos, int index) {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize
        int relpos = linkSize + index * (keySize + valueSize + linkSize);

        return getBytes(nodepos + relpos, keySize);
    }

    private long getKeyPosition(long nodepos, int index) {
        return nodepos + linkSize + index * (keySize + valueSize + linkSize);
    }


    /**
     * Get the value of the key with the given index in the node starting at the given node position
     * @param nodepos
     * @param index
     * @return
     */
    private byte[] getValueAt(long nodepos, int index) {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize,
        // then skip the size of the key and read valueSize bytes
        int relpos = linkSize + index * (keySize + valueSize + linkSize) + keySize;

        return getBytes(nodepos + relpos, valueSize);

    }

    private long getValuePosition(long nodepos, int index) {
        return nodepos + linkSize + index * (keySize + valueSize + linkSize) + keySize;
    }

    /**
     * Update the value of the key with the given index in the node with the given node position.
     *
     * @param nodepos node position of the node to update
     * @param index   index of the key inside the node
     * @param data    data to update
     * @throws IOException
     */
    private void setValueAt(long nodepos, int index, byte[] data) throws IOException {
        int relpos = linkSize + index * (keySize + valueSize + linkSize) + keySize;

        setBytes(nodepos + relpos, data);
    }

    /**
     * Get the position of the left node link of the key with the given index in the node starting at the given node
     * position.
     *
     * @param nodepos  node position of the node to query
     * @param index    index of the key inside the node
     * @return pointer pointing to the left child node of the key
     */
    private long getLeftNode(long nodepos, int index) {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize,
        // then go back the size of linkSize
        return readLong(nodepos + index * (keySize + valueSize + linkSize));
    }


    private void setLeftNode(long nodepos, int index, long child) throws IOException {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize,
        // then go back the size of linkSize
        writeLong(nodepos + index * (keySize + valueSize + linkSize), child);
    }

    /**
     * Get the position of the right node of the key with the given index in the node starting at the given node
     * position.
     *
     * @param nodepos  node position of the node to query
     * @param index    index of the key inside the node
     * @return pointer pointing to the right child node of the key
     */
    private long getRightNode(long nodepos, int index) {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize,
        // then go back the size of linkSize
        return readLong(nodepos + (index + 1) * (keySize + valueSize + linkSize));
    }

    private void setRightNode(long nodepos, int index, long child) throws IOException {
        // relative position: skip left link (8 byte long), then skip index times the sum of keySize, valueSize and linkSize,
        // then go back the size of linkSize
        writeLong(nodepos + (index + 1) * (keySize + valueSize + linkSize), child);
    }

    /**
     * Get the size of the node at the given position. The node size is stored in the last 4 bytes of the node block
     * buffer.
     *
     * @param nodepos starting position of node in byte buffer
     * @return current size of the node
     */
    private int getNodeSize(long nodepos) {
        return readInt(nodepos + nodeSize * (keySize + valueSize) + (nodeSize + 1) * 8);
    }

    /**
     * Update the node size of the node at the given position. The node size is stored in the last 4 bytes of the
     * node block buffer.
     *
     * @param nodepos starting position of node in byte buffer
     * @param newSize new node size
     * @throws IOException
     */
    private void setNodeSize(long nodepos, int newSize) throws IOException {
        writeInt(nodepos + nodeSize * (keySize + valueSize) + (nodeSize + 1) * 8, newSize);
    }


    /**
     * Find the value associated with the given key by traversing the b-tree starting at the node with the position
     * given as first argument. If the value is not found, returns null;
     *
     * @param nodepos
     * @param key
     * @return
     */
    private long findValue(long nodepos, byte[] key) {
        // perform search on current node
        // - if found: return
        // - if not found: get link for subnodes and continue there
        int curNodeSize = getNodeSize(nodepos);

        if(curNodeSize == 0) {
            return -1;
        }

        int lower = 0, middle = 0, upper = curNodeSize - 1;

        int _cmp = 0;

        long _key = -1;

        while (upper >= lower ) {
            middle = (upper + lower) >>> 1;

            _key   = getKeyPosition(nodepos, middle);

            _cmp   = compareArraysUnsigned(_key, key);
            if(_cmp == 0) {
                return getValuePosition(nodepos,middle);
            } else if(_cmp < 0) {
                lower = middle + 1;
            } else {
                upper = middle - 1;
            }

        }

        // not found; continue in child nodes; middle should be at the position of the separator node
        // we check the value of the comparison again and continue at either the left or the right link
        long link;
        if(_cmp < 0) {
            // separator was smaller than key, so we take the right link
            link = getRightNode(nodepos,middle);
        } else if(_cmp > 0) {
            link = getLeftNode(nodepos, middle);
        } else {
            throw new IllegalStateException("algorithm is in an invalid state; comparison was equal but value not returned");
        }
        if(link > 0) {
            return findValue(link,key);
        } else {
            return -1;
        }

    }


    /**
     * Internal implementation of get method, to be used by subclasses to implement their custom type-specific getter.
     * Takes the byte array representation of a key and returns the byte-array representation of the value if found,
     * or null otherwise,
     *
     * @param key byte array representation of the key
     * @return start position of the byte array representation of the assigned value, or -1 if not found
     */
    protected long getInternal(byte[] key) {
        return findValue(getRootPointer(),key);
    }

    /**
     * Find the path to the node containing the given key. Used for insert operations to
     * propagate inserts to parent nodes.
     *
     * @param nodepos
     * @param key
     * @param initial
     * @return
     */
    private List<Long> findLeafPath(long nodepos, byte[] key, List<Long> initial) {
        initial.add(nodepos);

        // perform search on current node
        // - if found: return
        // - if not found: get link for subnodes and continue there
        int curNodeSize = getNodeSize(nodepos);

        if(curNodeSize == 0) {
            return initial;
        }

        int lower = 0, middle = 0, upper = curNodeSize - 1;

        int _cmp = 0;

        byte[] _key = null;

        // add current node to path

        while (upper >= lower ) {
            middle = (upper + lower) >>> 1;

            _key   = getKeyAt(nodepos, middle);

            _cmp   = compareArraysUnsigned(_key, key);
            if(_cmp == 0) {
                return initial;
            } else if(_cmp < 0) {
                lower = middle + 1;
            } else {
                upper = middle - 1;
            }

        }
        // not found; continue in child nodes; middle should be at the position of the separator node
        // we check the value of the comparison again and continue at either the left or the right link
        long link;
        if(_cmp < 0) {
            // separator was smaller than key, so we take the right link
            link = getRightNode(nodepos,middle);
        } else if(_cmp > 0) {
            link = getLeftNode(nodepos, middle);
        } else {
            throw new IllegalStateException("algorithm is in an invalid state; comparison was equal but value not returned");
        }
        if(link > 0) {
            return findLeafPath(link, key, initial);
        } else {
            return initial; // leaf found
        }

    }

    /**
     * Update the value of the key given as argument using the new value and starting at the node with the given
     * starting position. Used in case the key is already present in the tree.
     *
     * @param nodepos
     * @param key
     * @param value
     * @return old value of the key (if found) or null
     * @throws IOException
     */
    private byte[] updateValue(long nodepos, byte[] key, byte[] value) throws IOException {
        // perform search on current node
        // - if found: return
        // - if not found: get link for subnodes and continue there
        int curNodeSize = getNodeSize(nodepos);

        if(curNodeSize == 0) {
            return null;
        }

        int lower = 0, middle = 0, upper = curNodeSize - 1;

        int _cmp = 0;

        byte[] _value = null;
        long _key = -1;

        while (upper >= lower ) {
            middle = (upper + lower) >>> 1;

            _key   = getKeyPosition(nodepos, middle);

            _cmp   = compareArraysUnsigned(_key, key);
            if(_cmp == 0) {
                _value = getValueAt(nodepos, middle);
                setValueAt(nodepos, middle, value);
                return _value;
            } else if(_cmp < 0) {
                lower = middle + 1;
            } else {
                upper = middle - 1;
            }

        }
        // not found; continue in child nodes; middle should be at the position of the separator node
        // we check the value of the comparison again and continue at either the left or the right link
        long link;
        if(_cmp < 0) {
            // separator was smaller than key, so we take the right link
            link = getRightNode(nodepos,middle);
        } else if(_cmp > 0) {
            link = getLeftNode(nodepos, middle);
        } else {
            throw new IllegalStateException("algorithm is in an invalid state; comparison was equal but value not returned");
        }
        if(link > 0) {
            return updateValue(link, key, value);
        } else {
            return null; // not found
        }

    }

    /**
     * Insert the key/value pair (with the given left and right node pointers) into the node with the position given
     * as argument. Splits the node if needed, in this case the separator node is propagated upwards to the parent
     * along the path. If the path is empty, a new root node is created.
     *
     * @param leafPos the position of the node where to insert the key/value
     * @param key     the key to insert
     * @param value   the value assigned with the key
     * @param leftLink the pointer to the left child node of the key (or 0L)
     * @param rightLink the pointer to the right child node of the key (or 0L)
     * @param path    the path from the root node leading to this entry
     * @throws IOException
     */
    private void insertValue(long leafPos, byte[] key, byte[] value, long leftLink, long rightLink, List<Long> path) throws IOException {

        int leafNodeSize = getNodeSize(leafPos);

        // check first if the key is already present; in this case, overwrite the existing value
        int lower = 0, middle = 0, upper = leafNodeSize - 1;

        int _cmp = 0;

        long _key = -1;

        while (upper >= lower ) {
            middle = (upper + lower) >>> 1;

            _key   = getKeyPosition(leafPos, middle);
            _cmp   = compareArraysUnsigned(_key, key);
            if(_cmp == 0) {
                throw new IllegalStateException("key found in node, but should already have been processed");
            } else if(_cmp < 0) {
                lower = middle + 1;
            } else {
                upper = middle - 1;
            }

        }

        // key was not found; now we have the following cases:
        // - current node is not yet full: insert and sort
        // - current node is full: evenly split into two nodes
        //   * chose the middle value as separator
        //   * move keys smaller than the separator into the left node, move keys bigger than the separator into the right node
        //   * move separator up to parent; if parent is already the root node, create a new root node and update the
        //     left and right link values
        if(leafNodeSize < nodeSize) {
            // the index "middle" points either to the left or to the right of where the new key needs to be inserted
            // depending on the value of _cmp

            if(log.isDebugEnabled()) {
                log.debug("inserting {}={} ({},{}) into node {}", getKeySerializer().apply(key), getKeySerializer().apply(value), leftLink, rightLink, leafPos);
            }

            int relidx = 0;
            if(_cmp < 0) {
                // key in array at middle was smaller, so we start inserting right of it
                relidx = middle+1;
            } else {
                relidx = middle;
            }
            int relpos   = relidx * (keySize + valueSize + linkSize);

            // number of bytes currently located starting at relidx; they need to be shifted right
            int remcount = (leafNodeSize - relidx) * (keySize + valueSize + linkSize) + linkSize;

            // shift existing content by the number of bytes needed for the key/value/link combination
            copyBytes(leafPos + relpos, leafPos + relpos + (keySize + valueSize + linkSize), remcount);

            // add new value to the now freed space, starting with the left pointer
            writeLong(leafPos + relpos, leftLink);
            setBytes(leafPos + relpos + linkSize, key);
            setBytes(leafPos + relpos + linkSize + keySize, value);

            // overwrite the right node link, this happens in case of a node split
            writeLong(leafPos + relpos + linkSize + keySize + valueSize, rightLink);

            // update node size
            setNodeSize(leafPos, leafNodeSize + 1);
        } else {
            // need to split at middle separator
            int separatorIdx = leafNodeSize / 2;
            byte[] separatorKey = getKeyAt(leafPos, separatorIdx);
            byte[] separatorValue = getValueAt(leafPos, separatorIdx);

            // move everything right of the separator key to a new node
            long right = createNewNode(leafPos, separatorIdx+1, leafNodeSize - separatorIdx - 1);

            if(log.isDebugEnabled()) {
                log.debug("splitting node {} at {} into ({},{})", leafPos, getKeySerializer().apply(separatorKey), leafPos, right);
            }




            // set size of the old leaf node to the new size
            setNodeSize(leafPos, separatorIdx);

            // propagate separator node together with its new children to parent node
            long parent = 0;
            List<Long> newPath;
            if(path.size() > 1) {
                // parent exists, we get it from the path list
                parent = path.get(path.size() - 2); // -1 is the current leaf
                newPath = path.subList(0,path.size() - 1);
            } else {
                // parent does not exist, create new root node and add it to the path list
                parent = createRootNode();
                newPath = Collections.singletonList(parent);
                maxLevel++;
            }
            insertValue(parent,separatorKey,separatorValue,leafPos,right,newPath);

            // if separatorKey is smaller, insert the current node in the right branch, otherwise in the left (leafPos)
            if(compareArraysUnsigned(separatorKey, key) < 0) {
                List<Long> childPath = new ArrayList<>(newPath);
                childPath.add(right);
                insertValue(right, key, value, leftLink, rightLink, childPath);
            } else {
                List<Long> childPath = new ArrayList<>(newPath);
                childPath.add(leafPos);
                insertValue(leafPos,key,value, leftLink, rightLink, childPath);
            }
        }

    }

     /**
     * Create a new node, copying over the data from
     * @param oldpos   old node position
     * @param oldidx   index in old node where to start from
     * @param oldcount number of key-values in old node to take
     * @return
     */
    private long createNewNode(long oldpos, int oldidx, int oldcount) throws IOException {
        // create a  node starting at the current end pointer, then move the endpointer by totalNodeSize
        long endPointer = getEndPointer();

        // start at the first byte of the left link of the oldidx element
        int relpos = oldidx * (keySize + valueSize + linkSize);

        // take the number of link-key-value pairs specified by oldcount, and the last link
        int bytecount = oldcount *(keySize + valueSize + linkSize) + linkSize;

        // copy over all old elements
        copyBytes(oldpos+relpos, endPointer, bytecount);

        // set correct node size
        setNodeSize(endPointer, oldcount);

        // update endpointer
        setEndPointer(endPointer + totalNodeSize);

        return endPointer;
    }

    /**
     * Create a new root node at the end of the buffer, updating the pointers to the root node and to the end node.
     *
     * @return position of the new root node in the byte buffer
     * @throws IOException
     */
    private long createRootNode() throws IOException {
        // create an empty node with all zeros starting at the current end pointer, then move the endpointer by totalNodeSize
        long endPointer = getEndPointer();
        for(int i=0; i<totalNodeSize; i++) {
            setByte(endPointer + i, (byte)0);
        }

        // update endpointer
        setEndPointer(endPointer + totalNodeSize);

        // update root pointer
        setRootPointer(endPointer);

        return endPointer;
    }

    /**
     * Add or update the value of the given key.
     *
     * @param key
     * @param value
     * @throws IOException
     */
    protected byte[] putInternal(byte[] key, byte[] value) throws IOException {
        byte[] oldvalue = updateValue(getRootPointer(), key, value);
        if(oldvalue != null) {
            return oldvalue;
        } else {
            // look for the path to the leaf that will contain this node
            List<Long> path = findLeafPath(getRootPointer(),key,new ArrayList<Long>(64));

            long leafPos = path.get(path.size()-1);

            insertValue(leafPos, key, value, 0L, 0L, path);

            return null;
        }
    }


    /**
     * Copy this B-Tree into a byte array. Warning, this can consume huge amounts of memory. For debugging
     * purposes only.
     *
     * @return
     */
    public byte[] toByteArray() {
        if(getEndPointer() < Integer.MAX_VALUE) {
            byte[] result = new byte[(int) getEndPointer()];
            for(int i=0;i< getEndPointer(); i++) {
                result[i] = getByte(i);
            }
            return result;
        } else {
            return null;
        }
    }

    /**
     * Print the tree starting at the given start node into a string. Uses the provided key and value functions for
     * creating a human-readable representation of the keys and values.
     *
     * @param keyFunction
     * @param valueFunction
     * @param prefix
     * @param start
     * @return
     */
    public String printTree(Function<byte[],String> keyFunction, Function<byte[], String> valueFunction, String prefix, long start) {
        StringBuilder builder = new StringBuilder();
        builder.append(prefix);
        builder.append("Node(pos=");
        builder.append(start);
        builder.append(",size=");
        builder.append(getNodeSize(start));
        builder.append(")[");
        for(int i=0; i<getNodeSize(start); i++) {
            builder.append(keyFunction.apply(getKeyAt(start, i)));
            builder.append("=");
            builder.append(valueFunction.apply(getValueAt(start,i)));
            if(i + 1 < getNodeSize(start)) {
                builder.append(",");
            }
        }
        builder.append("]\n");

        // print child nodes
        for(int i=0; i<getNodeSize(start)+1; i++) {
            if(getLeftNode(start, i) > 0) {
                builder.append(printTree(keyFunction, valueFunction, prefix + String.format("%3d. ", i), getLeftNode(start, i)));
            //} else {
            //    builder.append(prefix + String.format("%3d. (EMPTY)\n", i));
            }
        }

        return builder.toString();
    }


    /**
     * Return a serializer function suitable for creating human-readable representations of keys
     * @return
     */
    protected abstract Function<byte[], String> getKeySerializer();

    /**
     * Return a serializer function suitable for creating human-readable representations of values
     * @return
     */
    protected abstract Function<byte[], String> getValueSerializer();


    @Override
    public String toString() {
        return printTree(getKeySerializer(), getValueSerializer(), "", getRootPointer());
    }

    /**
     * Copy the complete B-Tree into another B-Tree (overwriting all values). The other B-Tree must have
     * the same properties as this B-Tree (i.e. key size, value size and node size).
     * @param other
     * @throws IOException
     */
    public void copyTo(MappedBTree other) throws IOException {
        if(this.nodeSize != other.nodeSize) {
            throw new IllegalArgumentException("node sizes differ, copying not possible");
        }
        if(this.keySize != other.keySize) {
            throw new IllegalArgumentException("key sizes differ, copying not possible");
        }
        if(this.valueSize != other.valueSize) {
            throw new IllegalArgumentException("value sizes differ, copying not possible");
        }


        for(long i = 0; i < getEndPointer(); i++) {
            other.setByte(i, this.getByte(i));
        }
        other.setRootPointer(this.getRootPointer());
        other.setEndPointer(this.getEndPointer());
        other.maxLevel = this.maxLevel;

    }


    /**
     * Return true in case the btree is empty.
     *
     * @return
     */
    public boolean isEmpty() {
        return getNodeCount() == 1 && getNodeSize(getRootPointer()) == 0;
    }


    /**
     * Apply the collector function given as argument on each key/value pair stored in the B-Tree using a
     * breadth-first strategy.
     *
     * @param collector collector function to apply to key/value entries
     * @param <T>       return type for each entry
     * @return
     */
    public <T> Iterator<T> collectBFS(CollectorFunction<T> collector) {
        return new BFSIterator<T>(collector);
    }


    public static interface CollectorFunction<T> {

        /**
         * Collect a key/value combination; also passes over the node id and the left and right children node ids
         * of the key/value combination.
         *
         * @param key
         * @param value
         * @param node
         * @param left
         * @param right
         * @return
         */
        public T collect(byte[] key, byte[] value, long node, int index, long left, long right);
    }



    private class BFSIterator<T> implements Iterator<T> {

        private Long currentNode = -1L;
        private int  currentIndex = -1;

        private Queue<Long> nodeQueue = new LinkedList<>();

        private CollectorFunction<T> collector;

        private BFSIterator(CollectorFunction<T> collector) {
            this.collector = collector;


            currentNode = getRootPointer();
            currentIndex = 0;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return !nodeQueue.isEmpty() || currentIndex < getNodeSize(currentNode) || getLeftNode(currentNode,currentIndex) != 0L;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws java.util.NoSuchElementException
         *          if the iteration has no more elements
         */
        @Override
        public T next() {
            if(!hasNext()) {
                throw new NoSuchElementException("no more elements in tree");
            }

            // add left child pointer to node queue; if currentIndex == nodeSize, will add the rightmost child
            long left = getLeftNode(currentNode, currentIndex);
            if(left != 0L) {
                nodeQueue.add(left);
            }

            if(currentIndex >= getNodeSize(currentNode)) {
                // end of current node reached, advance to next node
                currentNode  = nodeQueue.poll();
                currentIndex = 0;

                long left2 = getLeftNode(currentNode, currentIndex);
                if(left2 != 0L) {
                    nodeQueue.add(left2);
                }
            }

            log.debug("processing node {}, index {}, queue {}", currentNode, currentIndex, nodeQueue);

            // safeguard, should not be needed
            if(currentNode == null) {
                throw new NoSuchElementException("no more elements in tree");
            }

            // process current entry
            T result =  collector.collect(getKeyAt(currentNode,currentIndex), getValueAt(currentNode,currentIndex), currentNode, currentIndex, getLeftNode(currentNode,currentIndex), getRightNode(currentNode, currentIndex));

            // increase index counter
            currentIndex++;

            // return result
            return result;
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).  This method can be called
         * only once per call to {@link #next}.  The behavior of an iterator
         * is unspecified if the underlying collection is modified while the
         * iteration is in progress in any way other than by calling this
         * method.
         *
         * @throws UnsupportedOperationException if the {@code remove}
         *                                       operation is not supported by this iterator
         * @throws IllegalStateException         if the {@code next} method has not
         *                                       yet been called, or the {@code remove} method has already
         *                                       been called after the last call to the {@code next}
         *                                       method
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException("removal not supported");
        }
    }
}
