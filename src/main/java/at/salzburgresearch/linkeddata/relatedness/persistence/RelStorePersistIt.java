package at.salzburgresearch.linkeddata.relatedness.persistence;

import com.google.common.collect.Iterators;
import com.google.common.primitives.Ints;
import com.persistit.Exchange;
import com.persistit.Key;
import com.persistit.Persistit;
import com.persistit.exception.PersistitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelStorePersistIt extends RelStoreBase {

    private static Logger log = LoggerFactory.getLogger(RelStorePersistIt.class);

    // must be already initialized ...
    private Persistit database;

    private String name, reldb_name, adjdb_name;

    private ThreadLocal<Exchange> reldb_ex, adjdb_ex;

    public RelStorePersistIt(Persistit database, String name) {
        this.database = database;
        this.name     = name;
        this.reldb_name = name + "-main";
        this.adjdb_name = name + "-adj";

        this.reldb_ex = new ThreadLocal<Exchange>();
        this.adjdb_ex = new ThreadLocal<Exchange>();

    }

    protected void putRelatedness(int key, double value) {
        try {
            if(reldb_ex.get() == null) {
                reldb_ex.set(database.getExchange("reldb", this.reldb_name, true));
            }
            Exchange dbex = reldb_ex.get();

            dbex.getKey().to(key);
            dbex.getValue().put(value);

            dbex.store();

        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
    }

    protected double getRelatedness(int key) {
        try {
            if(reldb_ex.get() == null) {
                reldb_ex.set(database.getExchange("reldb", this.reldb_name, true));
            }
            Exchange dbex = reldb_ex.get();

            dbex.getKey().to(key);
            dbex.fetch();

            if(dbex.getValue().isDefined()) {
                return dbex.getValue().getDouble();
            }

        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
        return 0.0;
    }


    protected void updateAdjacency(int nodeIdX, int nodeIdY) {
        try {
            if(adjdb_ex.get() == null) {
                adjdb_ex.set(database.getExchange("reldb", this.adjdb_name, true));
            }
            Exchange dbex = adjdb_ex.get();

            dbex.getKey().to(nodeIdX);

            dbex.fetch();

            if(dbex.getValue().isDefined()) {
                int[] adjXOld = dbex.getValue().getIntArray();
                int idx = Arrays.binarySearch(adjXOld, nodeIdY);
                if(idx < 0) {
                    int[] adjXNew = new int[adjXOld.length + 1];
                    int posY = - idx - 1;
                    // copy over until posY
                    System.arraycopy(adjXOld,0,adjXNew,0,posY);
                    adjXNew[posY] = nodeIdY;
                    System.arraycopy(adjXOld,posY,adjXNew,posY+1,adjXOld.length-posY);
                    dbex.getValue().put(adjXNew);
                    dbex.store();
                }
            } else {
                dbex.getValue().put(new int[] {nodeIdY});
                dbex.store();
            }


        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
    }


    @Override
    protected Iterator<? extends HashPair> getRelatednessValues() {
        try {
            if(reldb_ex.get() == null) {
                reldb_ex.set(database.getExchange("reldb", this.reldb_name, true));
            }
            final Exchange dbex = reldb_ex.get();

            dbex.getKey().to(Key.BEFORE);

            return new Iterator<HashPair>() {
                @Override
                public boolean hasNext() {
                    try {
                        return dbex.hasNext();
                    } catch (PersistitException e) {
                        log.error("could not iterate over values",e);
                        return false;
                    }
                }

                @Override
                public HashPair next() {
                    try {
                        dbex.next();
                        return new HashPair(dbex.getKey().decodeInt(), dbex.getValue().getDouble());
                    } catch (PersistitException e) {
                        log.error("could not iterate over values", e);
                        return null;
                    }
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("removing not supported");
                }
            };


        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
        return Iterators.emptyIterator();
    }


    @Override
    public Iterator<? extends Adjacent> getNodes() {
        try {
            if(adjdb_ex.get() == null) {
                adjdb_ex.set(database.getExchange("reldb", this.adjdb_name, true));
            }
            final Exchange dbex = adjdb_ex.get();

            dbex.getKey().to(Key.BEFORE);

            return new Iterator<Adjacent>() {
                @Override
                public boolean hasNext() {
                    try {
                        return dbex.hasNext();
                    } catch (PersistitException e) {
                        log.error("could not iterate over values",e);
                        return false;
                    }
                }

                @Override
                public Adjacent next() {
                    try {
                        dbex.next();
                        return new Adjacent(dbex.getKey().decodeInt(), dbex.getValue().getIntArray());
                    } catch (PersistitException e) {
                        log.error("could not iterate over values", e);
                        return null;
                    }
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("removing not supported");
                }
            };


        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
        return Iterators.emptyIterator();
    }

    @Override
    public void setRelatedness(int nodeIdX, int nodeIdY, double relatedness) {
        putRelatedness(getRelationID(nodeIdX,nodeIdY), relatedness);

        updateAdjacency(nodeIdX, nodeIdY);
        updateAdjacency(nodeIdY, nodeIdX);
    }

    /**
     * Get the relatedness value between the node ids of the two uris passed as argument, if it exists. Returns
     * 0.0 otherwise.
     *
     * @param nodeIdX node ID of the first node
     * @param nodeIdY node ID of the second node
     * @return
     */
    @Override
    public double getRelatedness(int nodeIdX, int nodeIdY) {
        return getRelatedness(getRelationID(nodeIdX,nodeIdY));
    }

    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     *
     * @param nodeId
     * @return
     */
    @Override
    public int[] getAdjacent(int nodeId) {
        try {
            if(adjdb_ex.get() == null) {
                adjdb_ex.set(database.getExchange("reldb", this.adjdb_name, true));
            }
            Exchange dbex = adjdb_ex.get();

            dbex.getKey().to(nodeId);

            dbex.fetch();

            if(dbex.getValue().isDefined()) {
                return dbex.getValue().getIntArray();
            }
        } catch (PersistitException e) {
            log.error("error persisting relatedness data",e);
        }
        return new int[0];
    }

    @Override
    public boolean isEmpty() {
        try {
            if(reldb_ex.get() == null) {
                reldb_ex.set(database.getExchange("reldb", this.reldb_name, true));
            }
            Exchange dbex = reldb_ex.get();
            dbex.getKey().to(Key.BEFORE);
            return !dbex.hasNext();

        } catch (PersistitException e) {
            log.error("could not iterate over store", e);
            return true;
        }
    }

    /**
     * Copy all contents of this relatedness DB to the other relatedness DB.
     *
     * @param other
     */
    @Override
    public void copyTo(RelStore other) {
        if(other instanceof RelStorePersistIt) {
            RelStorePersistIt o = (RelStorePersistIt)other;

            try {
                // copy over main values
                Exchange myRelEx = database.getExchange("reldb", this.reldb_name, true);
                Exchange otRelEx = o.database.getExchange("reldb", this.reldb_name, true);

                myRelEx.getKey().to(Key.BEFORE);
                while(myRelEx.next()) {
                    otRelEx.getKey().to(myRelEx.getKey().decodeInt());
                    otRelEx.getValue().put(myRelEx.getValue().getDouble());
                    otRelEx.store();
                }

                // copy over adjacency values
                Exchange myAdjEx = database.getExchange("reldb", this.adjdb_name, true);
                Exchange otAdjEx = o.database.getExchange("reldb", this.adjdb_name, true);

                myAdjEx.getKey().to(Key.BEFORE);
                while(myAdjEx.next()) {
                    otAdjEx.getKey().to(myAdjEx.getKey().decodeInt());
                    otAdjEx.getValue().put(myAdjEx.getValue().getIntArray());
                    otAdjEx.store();
                }

            } catch (PersistitException e) {
                log.error("could not copy store data",e);
            }

        }
    }
}
