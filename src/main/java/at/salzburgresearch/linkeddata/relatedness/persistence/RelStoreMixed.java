package at.salzburgresearch.linkeddata.relatedness.persistence;

import com.google.common.collect.Iterators;
import com.google.common.primitives.Ints;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/**
 * Store relatedness information for different time points. The mixed implementation uses a combination of MapDB based
 * storage (for the relatedness values) and in-memory storage (for adjacency information).
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelStoreMixed extends RelStoreBase implements RelStore {

    private BTreeMap<Integer,Double> mapMain;
    private TIntObjectMap<int[]> adjMap;

    public RelStoreMixed(DB database, String prefix) {
        if(database.exists(prefix+"-main")) {
            mapMain = database.getTreeMap(prefix+"-main");
        } else {
            mapMain = database.createTreeMap(prefix+"-main").nodeSize(12).make();

        }
        adjMap = new TIntObjectHashMap<int[]>();
    }


    protected void updateAdjacency(int nodeIdX, int nodeIdY) {
        if(adjMap.containsKey(nodeIdX)) {
            int[] adjXOld = adjMap.get(nodeIdX);
            int idx = Arrays.binarySearch(adjXOld, nodeIdY);
            if(idx < 0) {
                int[] adjXNew = new int[adjXOld.length + 1];
                int posY = - idx - 1;
                // copy over until posY
                System.arraycopy(adjXOld,0,adjXNew,0,posY);
                adjXNew[posY] = nodeIdY;
                System.arraycopy(adjXOld, posY, adjXNew, posY + 1, adjXOld.length - posY);
                adjMap.put(nodeIdX, adjXNew);
            }
        } else {
            adjMap.put(nodeIdX, new int[]{nodeIdY});
        }
    }


    @Override
    protected Iterator<? extends HashPair> getRelatednessValues() {
        final Iterator<Map.Entry<Integer,Double>> wrapped = mapMain.entrySet().iterator();
        return new Iterator<HashPair>() {
            @Override
            public boolean hasNext() {
                return wrapped.hasNext();
            }

            @Override
            public HashPair next() {
                Map.Entry<Integer,Double> next = wrapped.next();
                return new HashPair(next.getKey(), next.getValue());
            }

            @Override
            public void remove() {
                wrapped.remove();
            }
        };
    }

    @Override
    public Iterator<? extends Adjacent> getNodes() {
        final TIntObjectIterator wrapped = adjMap.iterator();
        return new Iterator<Adjacent>() {
            @Override
            public boolean hasNext() {
                return wrapped.hasNext();
            }

            @Override
            public Adjacent next() {
                wrapped.advance();
                return new Adjacent(wrapped.key(), (int[])wrapped.value());
            }

            @Override
            public void remove() {
                wrapped.remove();
            }
        };
    }

    @Override
    public void setRelatedness(int nodeIdX, int nodeIdY, double relatedness) {
        if(mapMain.put(getRelationID(nodeIdX,nodeIdY), relatedness) == null) {
            // update adjacency index, because this is the first time we see this combination
            updateAdjacency(nodeIdX,nodeIdY);
            updateAdjacency(nodeIdY,nodeIdX);
        }
    }

    /**
     * Get the relatedness value between the node ids of the two uris passed as argument, if it exists. Returns
     * 0.0 otherwise.
     *
     * @param nodeIdX node ID of the first node
     * @param nodeIdY node ID of the second node
     * @return
     */
    @Override
    public double getRelatedness(int nodeIdX, int nodeIdY) {
        if(mapMain.containsKey(getRelationID(nodeIdX,nodeIdY))) {
            return mapMain.get(getRelationID(nodeIdX,nodeIdY));
        } else {
            return 0.0;
        }
    }

    /**
     * Return all node IDs of relations of the given URI (independent of the position the uri occurs in in the relation).
     *
     * @param nodeId
     * @return
     */
    @Override
    public int[] getAdjacent(int nodeId) {
        if(adjMap.containsKey(nodeId)) {
            return adjMap.get(nodeId);
        } else {
            return new int[0];
        }
    }

    @Override
    public boolean isEmpty() {
        return mapMain.isEmpty();
    }

    /**
     * Copy all contents of this relatedness DB to the other relatedness DB.
     *
     * @param other
     */
    @Override
    public void copyTo(RelStore other) {
        if(other instanceof RelStoreMixed) {
            RelStoreMixed op = (RelStoreMixed)other;
            for(Map.Entry<Integer,Double> entry : mapMain.entrySet()) {
                op.mapMain.put(entry.getKey(),entry.getValue());
            }
            TIntObjectIterator it2 = adjMap.iterator();
            while(it2.hasNext()) {
                it2.advance();
                int[] data = (int[])it2.value();
                op.adjMap.put(it2.key(), data);
            }

        } else {
            throw new IllegalArgumentException("cannot copy between different kinds of relatedness databases");
        }
    }

}
