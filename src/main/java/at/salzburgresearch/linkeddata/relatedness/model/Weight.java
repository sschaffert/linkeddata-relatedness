package at.salzburgresearch.linkeddata.relatedness.model;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class Weight {

    private String property;

    private double weight;

    public Weight() {
    }

    public Weight(String property, double weight) {
        this.property = property;
        this.weight = weight;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
