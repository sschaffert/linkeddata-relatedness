package at.salzburgresearch.linkeddata.relatedness.model;

import java.io.Serializable;
import java.text.Collator;

/**
 * Representation of a relation of two concepts. The order of the two concepts is irrelevant.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class Relation implements Serializable {

    private static final Collator collator = Collator.getInstance();

    private String conceptUri1, conceptUri2;

    private double relatedness;


    public Relation(String conceptUri1, String conceptUri2, double relatedness) {
        // order concept uris alphabetically, so we ensure that the order does not matter
        if(collator.compare(conceptUri1,conceptUri2) < 0) {
            this.conceptUri1 = conceptUri1;
            this.conceptUri2 = conceptUri2;
        } else {
            this.conceptUri2 = conceptUri1;
            this.conceptUri1 = conceptUri2;
        }
        this.relatedness = relatedness;
    }

    public String getConceptUri1() {
        return conceptUri1;
    }

    public String getConceptUri2() {
        return conceptUri2;
    }

    public double getRelatedness() {
        return relatedness;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Relation relation = (Relation) o;

        if (Double.compare(relation.relatedness, relatedness) != 0) return false;
        if (!conceptUri1.equals(relation.conceptUri1)) return false;
        if (!conceptUri2.equals(relation.conceptUri2)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = conceptUri1.hashCode();
        result = 31 * result + conceptUri2.hashCode();
        temp = Double.doubleToLongBits(relatedness);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public int getStanbolHash() {
        return conceptUri1.hashCode() * conceptUri2.hashCode();
    }

}
