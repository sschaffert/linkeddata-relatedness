package at.salzburgresearch.linkeddata.relatedness;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public enum Compression {
    NONE,
    ZIP,
    BZIP2,
    GZIP
}
