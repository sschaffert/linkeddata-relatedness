package at.salzburgresearch.linkeddata.relatedness;

import at.salzburgresearch.linkeddata.relatedness.algorithm.RelatednessAlgorithm;
import at.salzburgresearch.linkeddata.relatedness.algorithm.RelatednessAlgorithmDirect;
import at.salzburgresearch.linkeddata.relatedness.algorithm.RelatednessAlgorithmMemory;
import at.salzburgresearch.linkeddata.relatedness.algorithm.RelatednessAlgorithmRepository;
import at.salzburgresearch.linkeddata.relatedness.factory.BTreeFactory;
import at.salzburgresearch.linkeddata.relatedness.factory.FactoryException;
import at.salzburgresearch.linkeddata.relatedness.factory.MapDBFactory;
import at.salzburgresearch.linkeddata.relatedness.factory.MemoryFactory;
import at.salzburgresearch.linkeddata.relatedness.factory.MixedFactory;
import at.salzburgresearch.linkeddata.relatedness.factory.PersistItFactory;
import at.salzburgresearch.linkeddata.relatedness.factory.RelStoreFactory;
import at.salzburgresearch.linkeddata.relatedness.model.Weight;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.util.FileUtil;
import com.bigdata.Banner;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.event.InterceptingRepositoryConnection;
import org.openrdf.repository.event.base.InterceptingRepositoryConnectionWrapper;
import org.openrdf.repository.event.base.RepositoryConnectionInterceptorAdapter;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessUtil {

    private static Logger log = LoggerFactory.getLogger(RelatednessUtil.class);

    private File workDir;

    private Repository repository;

    public RelatednessUtil() {
        this(new File("."));
    }

    public RelatednessUtil(File workDir) {
        this.workDir = workDir;

        initDirectory();

        log.info("working directory: "+getWorkingDirectory().getAbsolutePath());
    }

    private void initDirectory() {
        File triples = getTriplesDirectory();
        triples.mkdirs();

        File work = getDBDirectory();
        work.mkdirs();

        File wsd = getWSDDirectory();
        wsd.mkdirs();

        File config = getConfigDirectory();
        config.mkdirs();

    }


    public File getWorkingDirectory() {
        return workDir;
    }

    public File getConfigDirectory() {
        return new File(getWorkingDirectory(), "config");
    }


    public File getTriplesDirectory() {
        return new File(getWorkingDirectory(), "triples");
    }

    public File getDBDirectory() {
        return new File(getWorkingDirectory(), "db");
    }

    public File getWSDDirectory() {
        return new File(getWorkingDirectory(), "wsd");
    }


    public Repository getRepository() throws RepositoryException {
        if(repository == null) {
            Properties properties = new Properties();
            properties.put("com.bigdata.rdf.store.AbstractTripleStore.statementIdentifiers", true);
            properties.put("com.bigdata.journal.AbstractJournal.bufferMode", "DiskRW");
            properties.put(BigdataSail.Options.FILE, new File(getTriplesDirectory(), "bigdata.jnl").getAbsolutePath());
            properties.setProperty(Banner.Options.LOG4J_MBEANS_DISABLE, "true");
            properties.setProperty(com.bigdata.btree.IndexMetadata.Options.WRITE_RETENTION_QUEUE_CAPACITY, "8000");

            BigdataSail sail = new BigdataSail(properties);
            repository =  new BigdataSailRepository(sail);
            repository.initialize();
        }
        return repository;
    }


    public void loadTriples(InputStream fin, RDFFormat format, Compression compression) throws RepositoryException, IOException, RDFParseException {
        InputStream in;
        switch (compression) {
            case BZIP2:
                in = new BufferedInputStream(new BZip2CompressorInputStream(fin, true));
                break;
            case GZIP:
                in = new BufferedInputStream(new GzipCompressorInputStream(fin, true));
                break;
            case ZIP:
                in = new BufferedInputStream(new ZipArchiveInputStream(fin));
                break;
            default:
                in = new BufferedInputStream(fin);
        }

        log.info("loading triples ... ");
        long start = System.currentTimeMillis();

        Repository repo = getRepository();

        InterceptingRepositoryConnection con = new InterceptingRepositoryConnectionWrapper(repository,repo.getConnection());
        con.setAutoCommit(false);

        // we are only interested in URI resources, no literals
        con.addRepositoryConnectionInterceptor(new RepositoryConnectionInterceptorAdapter() {
            @Override
            public boolean add(RepositoryConnection conn, Resource subject, URI predicate, Value object, Resource... contexts) {
                return !(subject instanceof URI) || !(object instanceof URI);
            }
        });

        try {
            con.add(in, "", format);

            con.commit();

            log.info("loading {} triples finished ({} ms)", con.size(), System.currentTimeMillis() - start);
        } catch(RepositoryException ex) {
            con.rollback();

            log.error("error while importing data", ex);
        } finally {
            con.close();
        }

    }

    /**
     * List the most frequently used properties in the repository
     * @param count
     */
    public Map<String,Integer> listTopProperties(int count) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
        Map<String,Integer> result = new HashMap<String, Integer>();
        Repository r = getRepository();

        RepositoryConnection con = r.getConnection();
        try {

            String qs = "SELECT ?p (count(?s) as ?count) WHERE { ?s ?p ?o . FILTER isURI(?o) } GROUP BY ?p ORDER BY DESC(?count) LIMIT "+count;

            log.warn(qs);

            TupleQuery query = con.prepareTupleQuery(QueryLanguage.SPARQL, qs);

            TupleQueryResult qresult = query.evaluate();
            while(qresult.hasNext()) {
                BindingSet row = qresult.next();
                result.put(row.getValue("p").stringValue(), Integer.parseInt(row.getValue("count").stringValue()));
            }
            qresult.close();

            con.commit();
        } catch(RepositoryException ex) {
            con.rollback();
        } finally {
            con.close();
        }

        return result;
    }

    /**
     * Write sample properties configuration file from the list of properties passed as argument
     * @param properties
     * @throws IOException
     */
    public void writeWeightsSampleConfig(Iterable<String> properties) throws IOException {
        File sampleFile = new File(getConfigDirectory(),"weights-sample.csv");

        FileWriter writer = new FileWriter(sampleFile);
        CsvBeanWriter csv = new CsvBeanWriter(writer, CsvPreference.STANDARD_PREFERENCE);
        //csv.writeHeader("property uri", "weight");

        CellProcessor[] processors = new CellProcessor[] {
                new NotNull(),
                new ParseDouble()
        };

        for(String p : properties) {
            csv.write(new Weight(p, 0.0), new String[] {"property", "weight"}, processors );
        }
        csv.close();

        log.info("sample weights file written to {}",sampleFile.getAbsolutePath());
    }


    public Map<String,Double> readWeightsConfig() throws IOException {
        Map<String,Double> result = new HashMap<String, Double>();

        File configFile = new File(getConfigDirectory(), "weights.csv");

        FileReader reader = new FileReader(configFile);
        CsvBeanReader csv = new CsvBeanReader(reader, CsvPreference.STANDARD_PREFERENCE);

        CellProcessor[] processors = new CellProcessor[] {
                new NotNull(),
                new ParseDouble()
        };

        Weight w;
        while((w = csv.read(Weight.class,new String[] {"property", "weight"}, processors)) != null) {
            result.put(w.getProperty(),w.getWeight());
        }
        csv.close();

        return result;
    }


    public void computeRelatedness(int passes, double threshold, PersistenceMode mode) throws IOException, RepositoryException, FactoryException {
        Map<String,Double> weights = readWeightsConfig();

        File workfile = new File(getDBDirectory(), "temp.db");

        if(workfile.exists()) {
            log.warn("work database already exists, possible data corruption");
        }

        RelStoreFactory factory = null;
        switch (mode) {
            case MEMORY:
                factory = new MemoryFactory();
                break;
            case MAPDB:
                factory = new MapDBFactory(workfile);
                break;
            case MIXED:
                factory = new MixedFactory(workfile);
                break;
            case PERSISTIT:
                factory = new PersistItFactory(workfile);
                break;
            case BTREE:
                factory = new BTreeFactory(getDBDirectory());
                break;
        }


        RelatednessAlgorithmRepository a = new RelatednessAlgorithmRepository(factory, getRepository(),weights);
        a.setThreshold(threshold);
        RelStore result = a.run(passes);


        File reldbfile = new File(getWSDDirectory(), "relatedness.db");
        if(reldbfile.exists()) {
            log.warn("result database already exists, possible data corruption");
        }

        log.info("exporting computed data into result database ({})", reldbfile.getAbsolutePath());
        long start = System.currentTimeMillis();
        result.exportTo(reldbfile);
        log.info("exported data in {} ms", System.currentTimeMillis()-start);

        if(factory != null) {
            factory.close();
        }
    }


    public void computeDirect(int passes, double threshold, PersistenceMode mode, File triples) throws IOException, RepositoryException, FactoryException {
        Map<String,Double> weights = readWeightsConfig();

        File workfile = new File(getDBDirectory(), "temp.db");

        if(workfile.exists()) {
            log.warn("work database already exists, possible data corruption");
        }

        RelStoreFactory factory = null;
        switch (mode) {
            case MEMORY:
                factory = new MemoryFactory();
                break;
            case MAPDB:
                factory = new MapDBFactory(workfile);
                break;
            case MIXED:
                factory = new MixedFactory(workfile);
                break;
            case PERSISTIT:
                factory = new PersistItFactory(workfile);
                break;
            case BTREE:
                factory = new BTreeFactory(getDBDirectory());
                break;
        }


        RelatednessAlgorithmDirect a = new RelatednessAlgorithmDirect(factory, triples,weights);
        a.setThreshold(threshold);
        RelStore result = a.run(passes);


        File reldbfile = new File(getWSDDirectory(), "relatedness.db");
        if(reldbfile.exists()) {
            log.warn("result database already exists, possible data corruption");
        }

        log.info("exporting computed data into result database ({})", reldbfile.getAbsolutePath());
        long start = System.currentTimeMillis();
        result.exportTo(reldbfile);
        log.info("exported data in {} ms", System.currentTimeMillis()-start);

        if(factory != null) {
            factory.close();
        }
    }


    public void computeMemory(int passes, double threshold, PersistenceMode mode, File triples) throws IOException, RepositoryException, FactoryException {
        Map<String,Double> weights = readWeightsConfig();

        File workfile = new File(getDBDirectory(), "temp.db");

        if(workfile.exists()) {
            log.warn("work database already exists, possible data corruption");
        }

        RelStoreFactory factory = null;
        switch (mode) {
            case MEMORY:
                factory = new MemoryFactory();
                break;
            case MAPDB:
                factory = new MapDBFactory(workfile);
                break;
            case MIXED:
                factory = new MixedFactory(workfile);
                break;
            case PERSISTIT:
                factory = new PersistItFactory(workfile);
                break;
            case BTREE:
                factory = new BTreeFactory(getDBDirectory());
                break;
        }


        RelatednessAlgorithm a = new RelatednessAlgorithmMemory(factory, triples,weights);
        a.setThreshold(threshold);
        RelStore result = a.run(passes);


        File reldbfile = new File(getWSDDirectory(), "relatedness.db");
        if(reldbfile.exists()) {
            log.warn("result database already exists, possible data corruption");
        }

        log.info("exporting computed data into result database ({})", reldbfile.getAbsolutePath());
        long start = System.currentTimeMillis();
        result.exportTo(reldbfile);
        log.info("exported data in {} ms", System.currentTimeMillis()-start);

        if(factory != null) {
            factory.close();
        }
    }

    public static void usage() {
        System.out.println("Usage:");
        System.out.println(" - RelatednessUtil load  <infile>");
        System.out.println("   loads a dataset into the triplestore; parser and compression format are determined based on file suffices");
        System.out.println();
        System.out.println(" - RelatednessUtil properties <count>");
        System.out.println("   selects the top properties from the triple store, lists them, and adds them to a prepared weight configuration file");
        System.out.println();
        System.out.println(" - RelatednessUtil relatedness <mode> <threshold> <passes>");
        System.out.println("   compute relatedness database on the triple store using the given number of passes; <mode> is one of 'mapdb', 'memory', 'btree' and 'mixed'");
        System.out.println();
        System.out.println(" - RelatednessUtil direct <mode> <threshold> <passes> <infile>");
        System.out.println("   compute relatedness database using the triples contained in the given infile running the given number of passes; <mode> is one of 'mapdb', 'memory' and 'mixed'");
        System.out.println("   this implementation will parse the input file for every passs");
        System.out.println();
        System.out.println(" - RelatednessUtil memory <mode> <threshold> <passes> <infile>");
        System.out.println("   compute relatedness database using the triples contained in the given infile running the given number of passes; <mode> is one of 'mapdb', 'memory' and 'mixed'");
        System.out.println("   this implementation will parse the input file only for the first passs");
    }


    public static void main(String[] args) throws IOException, RepositoryException, RDFParseException, QueryEvaluationException, MalformedQueryException, FactoryException {
        if(args.length > 0) {
            if("load".equals(args[0])) {
                if(args.length == 2) {
                    File infile = new File(args[1]);
                    if(infile.exists() && infile.canRead()) {
                        RelatednessUtil util = new RelatednessUtil();

                        Compression compression = FileUtil.getCompression(args[1]);
                        RDFFormat   format      = FileUtil.getRDFFormat(args[1]);

                        System.out.println("input file "+args[1]+": format "+ format + ", compression "+compression);

                        util.loadTriples(new FileInputStream(infile),format,compression);
                    } else {
                        System.err.println("cannot read input file " + args[1]);
                    }
                } else {
                    usage();
                }
            } else if("properties".equals(args[0])) {
                if(args.length == 2) {
                    RelatednessUtil util = new RelatednessUtil();

                    int count = Integer.parseInt(args[1]);

                    Map<String,Integer> properties = util.listTopProperties(count);
                    for(Map.Entry<String,Integer> entry : properties.entrySet()) {
                        System.out.println("property <"+entry.getKey()+">: "+entry.getValue());
                    }

                    util.writeWeightsSampleConfig(properties.keySet());
                } else {
                    usage();
                }
            } else if("relatedness".equals(args[0])) {
                if(args.length == 4) {
                    RelatednessUtil util = new RelatednessUtil();

                    int count = Integer.parseInt(args[3]);

                    PersistenceMode mode = PersistenceMode.valueOf(args[1].toUpperCase());

                    double threshold = Double.parseDouble(args[2]);

                    util.computeRelatedness(count,threshold, mode);
                } else {
                    usage();
                }
            } else if("direct".equals(args[0])) {
                if(args.length == 5) {
                    File infile = new File(args[4]);
                    if(infile.exists() && infile.canRead()) {
                        RelatednessUtil util = new RelatednessUtil();

                        int count = Integer.parseInt(args[3]);

                        PersistenceMode mode = PersistenceMode.valueOf(args[1].toUpperCase());

                        double threshold = Double.parseDouble(args[2]);

                        util.computeDirect(count,threshold, mode, infile);
                    } else {
                        System.err.println("cannot read input file " + args[1]);
                    }
                } else {
                    usage();
                }
            } else if("memory".equals(args[0])) {
                if(args.length == 5) {
                    File infile = new File(args[4]);
                    if(infile.exists() && infile.canRead()) {
                        RelatednessUtil util = new RelatednessUtil();

                        int count = Integer.parseInt(args[3]);

                        PersistenceMode mode = PersistenceMode.valueOf(args[1].toUpperCase());

                        double threshold = Double.parseDouble(args[2]);

                        util.computeMemory(count,threshold, mode, infile);
                    } else {
                        System.err.println("cannot read input file " + args[1]);
                    }
                } else {
                    usage();
                }
            }
        } else {
            usage();
        }
    }

}
