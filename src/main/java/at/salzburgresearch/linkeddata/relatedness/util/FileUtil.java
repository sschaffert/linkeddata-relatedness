package at.salzburgresearch.linkeddata.relatedness.util;

import at.salzburgresearch.linkeddata.relatedness.Compression;
import org.apache.commons.compress.compressors.bzip2.BZip2Utils;
import org.apache.commons.compress.compressors.gzip.GzipUtils;
import org.apache.commons.io.FilenameUtils;
import org.openrdf.rio.RDFFormat;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class FileUtil {
    public static Compression getCompression(String fileName) {
        if(BZip2Utils.isCompressedFilename(fileName)) {
            return Compression.BZIP2;
        } else if(GzipUtils.isCompressedFilename(fileName)) {
            return Compression.GZIP;
        } else if(fileName.endsWith(".zip")) {
            return Compression.ZIP;
        } else {
            return Compression.NONE;
        }
    }

    public static RDFFormat getRDFFormat(String fileName) {
        return RDFFormat.forFileName(fileName, RDFFormat.forFileName(FilenameUtils.removeExtension(fileName)));
    }
}
