package at.salzburgresearch.linkeddata.relatedness;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public enum PersistenceMode {
    MAPDB, MEMORY, MIXED, PERSISTIT, BTREE
}
