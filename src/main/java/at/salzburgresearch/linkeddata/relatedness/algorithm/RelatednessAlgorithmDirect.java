package at.salzburgresearch.linkeddata.relatedness.algorithm;

import at.salzburgresearch.linkeddata.relatedness.Compression;
import at.salzburgresearch.linkeddata.relatedness.factory.FactoryException;
import at.salzburgresearch.linkeddata.relatedness.factory.RelStoreFactory;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.util.FileUtil;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.RDFHandlerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Implementation of our Linked Data Relatedness Algorithm. Allows running a specified number of passes
 * or running until no more additions are made over a certain threshold.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessAlgorithmDirect implements RelatednessAlgorithm {

    private static Logger log = LoggerFactory.getLogger(RelatednessAlgorithmDirect.class);

    private RelStoreFactory factory;

    private Map<String,Double> weights;

    private File triples;

    private double threshold = 0.0;

    private RDFFormat format;

    private Compression compression;

    /**
     * Create a new instance of the Linked Data Relatedness Algorithm using the db factory and the repository
     * given as arguments. The database should initially be empty, the repository should contain all considered
     * triples, the weights map for each considered property the associated weight. The MapDB database will be
     * used for storing the data of each pass and can be deleted once the results have been obtained.
     * <p/>
     * Only use this constructor if you have sufficient heap memory configured!
     *
     * @param triples     a file containing triples for finding relatedness; format and compression are determined
     *                    automatically based on file suffixes
     * @param weights     a map from property URIs to double weights used for determining relatedness
     */
    public RelatednessAlgorithmDirect(RelStoreFactory factory, File triples, Map<String, Double> weights) {
        this.factory     = factory;
        this.triples     = triples;
        this.weights     = weights;

        this.format      = FileUtil.getRDFFormat(triples.getName());
        this.compression = FileUtil.getCompression(triples.getName());
    }



    @Override
    public double getThreshold() {
        return threshold;
    }

    @Override
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    /**
     * Run the given number of passes and return a reference to a RelatednessDBPersistent object with the results of the
     * latest pass.
     *
     * @param numberOfPasses
     * @return
     */
    @Override
    public RelStore run(int numberOfPasses) throws RepositoryException, FactoryException, IOException {

        log.info("starting LDR computation ...");
        log.info("- passes:      {}", numberOfPasses);
        log.info("- threshold:   {}", threshold);
        log.info("- persistence: {}", factory.getName());

        RelStore result = null;
        for(int i=0; i<= numberOfPasses; i++) {
            result = runPass(i, result);
        }
        return result;
    }

    private RelStore runPass(int pass, RelStore previous) throws IOException, FactoryException, RepositoryException {
        FileInputStream fin = new FileInputStream(triples);

        InputStream in;
        switch (compression) {
            case BZIP2:
                in = new BufferedInputStream(new BZip2CompressorInputStream(fin, true));
                break;
            case GZIP:
                in = new BufferedInputStream(new GzipCompressorInputStream(fin, true));
                break;
            case ZIP:
                in = new BufferedInputStream(new ZipArchiveInputStream(fin));
                break;
            default:
                in = new BufferedInputStream(fin);
        }

        if(pass < 0) {
            throw new IllegalArgumentException("pass must be >= 0");
        }

        log.info("LDR algorithm pass {} ...", pass);


        RelStore reldb_old = previous;

        if(pass > 0) {
            if(reldb_old == null) {
                reldb_old = factory.createRelatednessDB(getMapName(pass - 1));
            }
            if(reldb_old.isEmpty()) {
                throw new IllegalStateException("database for previous pass does not exist!");
            }
        }

        log.info("LDR algorithm pass {}: opening database for this pass", pass);
        RelStore reldb_new = factory.createRelatednessDB(getMapName(pass));

        if(pass > 0) {
            log.info("LDR algorithm pass {}: copying over previous pass data", pass);
            // step 1: copy over all old entries to the new database
            long startCopy = System.currentTimeMillis();
            reldb_old.copyTo(reldb_new);
            log.info("LDR algorithm pass {}: copied previous pass data ({} ms)", pass, System.currentTimeMillis()-startCopy);
        }


        RDFParser parser = Rio.createParser(format);
        parser.setRDFHandler(new LDRHandler(reldb_new, reldb_old, pass));
        try {
            parser.parse(in,"");
        } catch (RDFParseException e) {
            throw new RepositoryException("could not parse input data",e);
        } catch (RDFHandlerException e) {
            throw new RepositoryException("could not handle input data",e);
        }

        return reldb_new;
    }


    /**
     * Generate the name for the map of the given pass in the MapDB database
     * @param pass
     * @return
     */
    private String getMapName(int pass) {
        return "reldb-"+pass;
    }



    private class LDRHandler extends RDFHandlerBase implements RDFHandler {

        private int pass = 0;

        private RelStore reldb_old, reldb_new;

        private long count = 0;

        private long startIndex = System.currentTimeMillis();

        private LDRHandler(RelStore reldb_new, RelStore reldb_old, int pass) {
            this.reldb_new = reldb_new;
            this.reldb_old = reldb_old;
            this.pass = pass;
        }

        @Override
        public void handleStatement(Statement stmt) throws RDFHandlerException {
            Double weight = weights.get(stmt.getPredicate().stringValue());

            if(weight != null && weight > threshold) {
                if(pass == 0) {
                    if(stmt.getSubject() instanceof URI && stmt.getObject() instanceof URI) {
                        int nodeIdX = reldb_new.getNodeID(stmt.getSubject().stringValue());
                        int nodeIdY = reldb_new.getNodeID(stmt.getObject().stringValue());

                        if(nodeIdX != nodeIdY) {
                            double weight_old = reldb_new.getRelatedness(nodeIdX, nodeIdY);

                            if(weight > weight_old) {
                                reldb_new.setRelatedness(nodeIdX, nodeIdY, weight);

                                count++;
                                if(count % 100000 == 0) {
                                    log.info("LDR algorithm pass 0: {} relations processed ({}/sec)", count, 100000 * 1000 / (System.currentTimeMillis() - startIndex));
                                    startIndex = System.currentTimeMillis();
                                }
                            }
                        }
                    }
                } else {
                    if(stmt.getSubject() instanceof URI && stmt.getObject() instanceof URI) {
                        int nodeIdX = reldb_new.getNodeID(stmt.getSubject().stringValue());
                        int nodeIdY = reldb_new.getNodeID(stmt.getObject().stringValue());

                        // we have a relation X p Y, let's look for Y _ Z in the old relation database
                        for(int nodeIdZ : reldb_old.getAdjacent(nodeIdY)) {

                            if(nodeIdZ != nodeIdX) {
                                double weight_old = reldb_new.getRelatedness(nodeIdX, nodeIdZ);

                                if(weight > weight_old) { // optimization to avoid unnecessary lookups
                                    double weight_new = reldb_old.getRelatedness(nodeIdY, nodeIdZ) * weight;

                                    if(weight_new > weight_old && weight_new > threshold) {
                                        reldb_new.setRelatedness(nodeIdX, nodeIdZ, weight_new);

                                        count++;
                                        if(count % 100000 == 0) {
                                            log.info("LDR algorithm pass {}: {} relations processed ({}/sec)", pass, count, 100000 * 1000 / (System.currentTimeMillis() - startIndex));
                                            startIndex = System.currentTimeMillis();
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}
