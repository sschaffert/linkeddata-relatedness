package at.salzburgresearch.linkeddata.relatedness.algorithm;

import at.salzburgresearch.linkeddata.relatedness.factory.FactoryException;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import org.openrdf.repository.RepositoryException;

import java.io.IOException;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface RelatednessAlgorithm {
    double getThreshold();

    void setThreshold(double threshold);

    /**
     * Run the given number of passes and return a reference to a RelatednessDBPersistent object with the results of the
     * latest pass.
     *
     * @param numberOfPasses
     * @return
     */
    RelStore run(int numberOfPasses) throws RepositoryException, FactoryException, IOException;
}
