package at.salzburgresearch.linkeddata.relatedness.algorithm;

import at.salzburgresearch.linkeddata.relatedness.factory.FactoryException;
import at.salzburgresearch.linkeddata.relatedness.factory.RelStoreFactory;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Implementation of our Linked Data Relatedness Algorithm. Allows running a specified number of passes
 * or running until no more additions are made over a certain threshold.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessAlgorithmRepository implements RelatednessAlgorithm {

    private static Logger log = LoggerFactory.getLogger(RelatednessAlgorithmRepository.class);

    private RelStoreFactory factory;

    private Repository repository;

    private Map<String,Double> weights;

    private double threshold = 0.0;

    /**
     * Create a new instance of the Linked Data Relatedness Algorithm using the db factory and the repository
     * given as arguments. The database should initially be empty, the repository should contain all considered
     * triples, the weights map for each considered property the associated weight. The MapDB database will be
     * used for storing the data of each pass and can be deleted once the results have been obtained.
     * <p/>
     * Only use this constructor if you have sufficient heap memory configured!
     *
     * @param repository  the triple store to work on for finding relatedness
     * @param weights     a map from property URIs to double weights used for determining relatedness
     */
    public RelatednessAlgorithmRepository(RelStoreFactory factory, Repository repository, Map<String, Double> weights) {
        this.factory    = factory;
        this.repository = repository;
        this.weights    = weights;
    }



    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    /**
     * Run the given number of passes and return a reference to a RelatednessDBPersistent object with the results of the
     * latest pass.
     *
     * @param numberOfPasses
     * @return
     */
    public RelStore run(int numberOfPasses) throws RepositoryException, FactoryException {

        log.info("starting LDR computation ...");
        log.info("- passes:      {}", numberOfPasses);
        log.info("- threshold:   {}", threshold);
        log.info("- persistence: {}", factory.getName());

        RelStore result = null;
        for(int i=0; i<= numberOfPasses; i++) {
            result = runPass(i, result);
        }
        return result;
    }


    protected RelStore init() throws RepositoryException, FactoryException {
        log.info("LDR algorithm init ...");
        long start = System.currentTimeMillis();

        RelStore reldb0 = factory.createRelatednessDB(getMapName(0));

        long count = 0;
        long startIndex = System.currentTimeMillis();
        try {
            RepositoryConnection con = repository.getConnection();
            try {

                for(Map.Entry<String,Double> weight : weights.entrySet()) {
                    if(weight.getValue() > threshold) {
                        URI property = repository.getValueFactory().createURI(weight.getKey());

                        RepositoryResult <Statement> stmts = con.getStatements(null,property,null,true);
                        while(stmts.hasNext()) {
                            Statement next = stmts.next();
                            if(next.getSubject() instanceof URI && next.getObject() instanceof URI) {
                                int nodeIdX = reldb0.getNodeID(next.getSubject().stringValue());
                                int nodeIdY = reldb0.getNodeID(next.getObject().stringValue());

                                if(nodeIdX != nodeIdY) {
                                    double weight_old = reldb0.getRelatedness(nodeIdX, nodeIdY);

                                    if(weight.getValue() > weight_old) {
                                        reldb0.setRelatedness(nodeIdX, nodeIdY, weight.getValue());

                                        count++;
                                        if(count % 100000 == 0) {
                                            log.info("LDR algorithm pass 0: {} relations processed ({}/sec)", count, 100000 * 1000 / (System.currentTimeMillis() - startIndex));
                                            startIndex = System.currentTimeMillis();
                                        }
                                    }
                                }
                            }
                        }
                        stmts.close();
                    }
                }
                con.commit();
            } catch(RepositoryException ex) {
                con.rollback();
            } finally {
                con.close();
            }
        } catch(RepositoryException ex) {
            throw ex;
        }
        log.info("LDR algorithm init finished: {} relations processed, {} ms", count, System.currentTimeMillis() - start);

        return reldb0;
    }


    protected RelStore runPass(int pass, RelStore previous) throws RepositoryException, FactoryException {
        if(pass == 0) {
            return init();
        }

        if(pass < 0) {
            throw new IllegalArgumentException("pass must be >= 0");
        }

        log.info("LDR algorithm pass {} ...", pass);
        long count = 0;
        long start = System.currentTimeMillis();

        RelStore reldb_old = previous;

        if(reldb_old == null) {
            reldb_old = factory.createRelatednessDB(getMapName(pass - 1));
        }
        if(reldb_old.isEmpty()) {
            throw new IllegalStateException("database for previous pass does not exist!");
        }

        log.info("LDR algorithm pass {}: opening database for this pass", pass);
        RelStore reldb_new = factory.createRelatednessDB(getMapName(pass));

        log.info("LDR algorithm pass {}: copying over previous pass data", pass);
        // step 1: copy over all old entries to the new database
        long startCopy = System.currentTimeMillis();
        reldb_old.copyTo(reldb_new);
        log.info("LDR algorithm pass {}: copied previous pass data ({} ms)", pass, System.currentTimeMillis()-startCopy);

        long startIndex = System.currentTimeMillis();
        try {
            RepositoryConnection con = repository.getConnection();
            try {
                int nodeIdX, nodeIdY;
                Statement next;

                for(Map.Entry<String,Double> weight : weights.entrySet()) {
                    URI property = repository.getValueFactory().createURI(weight.getKey());

                    RepositoryResult <Statement> stmts = con.getStatements(null,property,null,true);
                    while(stmts.hasNext()) {
                        next = stmts.next();
                        if(next.getSubject() instanceof URI && next.getObject() instanceof URI) {
                            nodeIdX = reldb_new.getNodeID(next.getSubject().stringValue());
                            nodeIdY = reldb_new.getNodeID(next.getObject().stringValue());

                            // we have a relation X p Y, let's look for Y _ Z in the old relation database

                            for(int nodeIdZ : reldb_old.getAdjacent(nodeIdY)) {

                                if(nodeIdZ != nodeIdX) {
                                    double weight_old = reldb_new.getRelatedness(nodeIdX, nodeIdZ);

                                    if(weight.getValue() > weight_old) { // optimization to avoid unnecessary lookups
                                        double weight_new = reldb_old.getRelatedness(nodeIdY, nodeIdZ) * weight.getValue();

                                        if(weight_new > weight_old && weight_new > threshold) {
                                            reldb_new.setRelatedness(nodeIdX, nodeIdZ, weight_new);

                                            count++;
                                            if(count % 100000 == 0) {
                                                log.info("LDR algorithm pass {}: {} relations processed ({}/sec)", pass, count, 100000 * 1000 / (System.currentTimeMillis() - startIndex));
                                                startIndex = System.currentTimeMillis();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    stmts.close();
                }

                con.commit();
            } catch(RepositoryException ex) {
                con.rollback();
            } finally {
                con.close();
            }
        } catch(RepositoryException ex) {
            throw ex;
        }

        log.info("LDR algorithm pass {} finished: {} relations processed ({} ms)", pass, count, System.currentTimeMillis() - start);

        return reldb_new;
    }



    /**
     * Generate the name for the map of the given pass in the MapDB database
     * @param pass
     * @return
     */
    private String getMapName(int pass) {
        return "reldb-"+pass;
    }
}
