package at.salzburgresearch.linkeddata.relatedness.factory;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreBTree;

import java.io.File;
import java.io.IOException;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class BTreeFactory implements RelStoreFactory {

    File workDir;

    public BTreeFactory(File workDir) {
        this.workDir = workDir;
    }

    /**
     * Close the factory, cleaning up any claimed resources
     */
    @Override
    public void close() {
        // nothing to do, garbage collector will clean up
    }

    /**
     * Create a new relatedness DB with the given name.
     *
     * @param name
     * @return
     */
    @Override
    public RelStore createRelatednessDB(String name) throws FactoryException {
        try {
            return new RelStoreBTree(workDir, name);
        } catch (IOException e) {
            throw new FactoryException("could not create b-tree database", e);
        }
    }

    /**
     * Return a human-readable name for this factory
     *
     * @return
     */
    @Override
    public String getName() {
        return "B-Tree";
    }
}
