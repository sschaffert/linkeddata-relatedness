package at.salzburgresearch.linkeddata.relatedness.factory;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface RelStoreFactory {


    /**
     * Create a new relatedness DB with the given name.
     *
     * @param name
     * @return
     */
    public RelStore createRelatednessDB(String name) throws FactoryException;


    /**
     * Return a human-readable name for this factory
     * @return
     */
    public String getName();


    /**
     * Close the factory, cleaning up any claimed resources
     */
    public void close();
}
