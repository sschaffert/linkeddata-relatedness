package at.salzburgresearch.linkeddata.relatedness.factory;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreMemory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MemoryFactory implements RelStoreFactory {

    private static Logger log = LoggerFactory.getLogger(MemoryFactory.class);

    /**
     * Create a new relatedness DB with the given name.
     *
     * @param name
     * @return
     */
    @Override
    public RelStore createRelatednessDB(String name) {
        log.info("creating new memory based relatedness database with name {}", name);
        return new RelStoreMemory();
    }

    /**
     * Return a human-readable name for this factory
     *
     * @return
     */
    @Override
    public String getName() {
        return "memory";
    }

    /**
     * Close the factory, cleaning up any claimed resources
     */
    @Override
    public void close() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
