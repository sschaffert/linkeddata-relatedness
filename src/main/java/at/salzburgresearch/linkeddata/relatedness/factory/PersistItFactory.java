package at.salzburgresearch.linkeddata.relatedness.factory;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStorePersistIt;
import com.google.common.collect.Lists;
import com.persistit.Configuration;
import com.persistit.Persistit;
import com.persistit.VolumeSpecification;
import com.persistit.exception.PersistitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class PersistItFactory implements RelStoreFactory {

    private static Logger log = LoggerFactory.getLogger(PersistItFactory.class);

    // must be already initialized ...
    private Persistit database;

    public PersistItFactory(Persistit database) {
        this.database = database;
    }

    public PersistItFactory(File dbfile) throws FactoryException {
        File dir = dbfile.getParentFile();
        File journal = new File(dir,"journal");
        File reldb   = new File(dir,"rel.db");
        File sysdb   = new File(dir,"sys.db");

        long maxPages = (10L * 1024L * 1024L * 1024L) / 2048L;

        log.info("creating PersistIt database in {}", dir.getAbsolutePath());
        Configuration cfg = new Configuration();
        cfg.setJournalPath(journal.getAbsolutePath());
        VolumeSpecification v_reldb = new VolumeSpecification(reldb.getAbsolutePath(), "reldb", 2048, 1024, maxPages, 1024, true, false, false);
        VolumeSpecification v_sys = new VolumeSpecification(sysdb.getAbsolutePath(), "_system", 2048, 128, 4096, 128, true, false, false);
        cfg.setVolumeList(Lists.newArrayList(v_sys, v_reldb));
        cfg.getBufferPoolMap().get(2048).setCount(800000);

        //cfg.setJmxEnabled(false);

        try {
            database = new Persistit();
            database.setConfiguration(cfg);
            database.initialize();
        } catch (PersistitException e) {
            log.error("could not initialize PersistIt database", e);

            throw new FactoryException(e);
        }
    }

    /**
     * Create a new relatedness DB with the given name.
     *
     * @param name
     * @return
     */
    @Override
    public RelStore createRelatednessDB(String name) {
        return new RelStorePersistIt(database, name);
    }

    /**
     * Return a human-readable name for this factory
     *
     * @return
     */
    @Override
    public String getName() {
        return "PersistIt";
    }

    /**
     * Close the factory, cleaning up any claimed resources
     */
    @Override
    public void close() {
        try {
            this.database.close();
        } catch (PersistitException e) {
            log.error("error closing PersistIt database", e);
        }
    }

}
