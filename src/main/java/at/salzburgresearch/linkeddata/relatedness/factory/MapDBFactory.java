package at.salzburgresearch.linkeddata.relatedness.factory;

import at.salzburgresearch.linkeddata.relatedness.persistence.RelStore;
import at.salzburgresearch.linkeddata.relatedness.persistence.RelStoreMapDB;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class MapDBFactory implements RelStoreFactory {

    private static Logger log = LoggerFactory.getLogger(MapDBFactory.class);

    private DB database;

    public MapDBFactory(DB database) {
        this.database = database;
    }

    public MapDBFactory(File dbfile) {
        this.database = DBMaker
                .newFileDB(dbfile)
                .closeOnJvmShutdown()
                .cacheSoftRefEnable()
                .transactionDisable()
                .asyncFlushDelay(1000)
//                .compressionEnable()
                .make();
    }

    /**
     * Create a new relatedness DB with the given name.
     *
     * @param name
     * @return
     */
    @Override
    public RelStore createRelatednessDB(String name) {
        log.info("creating new MapDB based relatedness database with name {}", name);
        return new RelStoreMapDB(database, name);
    }

    /**
     * Return a human-readable name for this factory
     *
     * @return
     */
    @Override
    public String getName() {
        return "MapDB";
    }

    /**
     * Close the factory, cleaning up any claimed resources
     */
    @Override
    public void close() {
        this.database.close();
    }
}
