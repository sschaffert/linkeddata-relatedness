About
=====

This repository contains tools for computing the relatedness of Linked Data
resources using different approaches. This data is used e.g. in Apache Stanbol
for disambiguation of ambiguous link suggestions, but can also be applied in
different contexts.